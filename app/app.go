package app

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// App instance
type App struct {
	Router *mux.Router
	N      *negroni.Negroni
}

// RunApplication setup the routing and initialze the http server
func (app *App) RunApplication(address string) {
	// Initialize routing
	app.InitializeRouting()
	// Setup the negroni middleware
	app.N = negroni.Classic()
	// Use the initialized router
	app.N.UseHandler(app.Router)
	// Listen to http connections
	http.ListenAndServe(address, app.N)
}
