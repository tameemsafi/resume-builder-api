package app

import (
	"resume-api/controllers"

	"resume-api/utils"

	"fmt"

	"net/http"

	"github.com/gorilla/mux"
)

// InitializeRouting Add all the routes to the application
func (app *App) InitializeRouting() {
	// Create a new router instance
	router := mux.NewRouter()
	// Store router instance in the app object
	app.Router = router

	// API status route
	app.addUnRestrictedRoute("status", "GET", controllers.StatusController.Status)

	// Authentication routes
	app.addUnRestrictedRoute("register", "POST", controllers.AuthController.Create)
	app.addUnRestrictedRoute("login", "POST", controllers.AuthController.Login)
	app.addRestrictedRoute("generate-token", "GET", controllers.AuthController.GenerateToken)

	// Profile routes
	app.addRestrictedRoute("avatar", "POST", controllers.ProfileController.UploadAvatar)
	app.addRestrictedRoute("profile", "PUT", controllers.ProfileController.UpdateProfile)

	// Work history routes
	app.addRestrictedRoute("work-histories", "GET", controllers.WorkHistoryController.Read)
	app.addRestrictedRoute("work-history", "POST", controllers.WorkHistoryController.Create)
	app.addRestrictedRoute("work-history/{id}", "PUT", controllers.WorkHistoryController.Update)
	app.addRestrictedRoute("work-history/{id}", "DELETE", controllers.WorkHistoryController.Delete)

	// Education routes
	app.addRestrictedRoute("educations", "GET", controllers.EducationController.Read)
	app.addRestrictedRoute("education", "POST", controllers.EducationController.Create)
	app.addRestrictedRoute("education/{id}", "PUT", controllers.EducationController.Update)
	app.addRestrictedRoute("education/{id}", "DELETE", controllers.EducationController.Delete)

	// Award/Certification routes
	app.addRestrictedRoute("awards", "GET", controllers.AwardController.Read)
	app.addRestrictedRoute("award", "POST", controllers.AwardController.Create)
	app.addRestrictedRoute("award/{id}", "PUT", controllers.AwardController.Update)
	app.addRestrictedRoute("award/{id}", "DELETE", controllers.AwardController.Delete)

	// Skills routes
	app.addRestrictedRoute("skills", "GET", controllers.SkillController.Read)
	app.addRestrictedRoute("skill", "POST", controllers.SkillController.Create)
	app.addRestrictedRoute("skill/{id}", "PUT", controllers.SkillController.Update)
	app.addRestrictedRoute("skill/{id}", "DELETE", controllers.SkillController.Delete)

	// Languages
	app.addRestrictedRoute("languages", "GET", controllers.LanguageController.Read)
	app.addRestrictedRoute("language", "POST", controllers.LanguageController.Create)
	app.addRestrictedRoute("language/{id}", "PUT", controllers.LanguageController.Update)
	app.addRestrictedRoute("language/{id}", "DELETE", controllers.LanguageController.Delete)

	// Hobbies routes
	app.addRestrictedRoute("hobbies", "GET", controllers.HobbieController.Read)
	app.addRestrictedRoute("hobbie", "POST", controllers.HobbieController.Create)
	app.addRestrictedRoute("hobbie/{id}", "PUT", controllers.HobbieController.Update)
	app.addRestrictedRoute("hobbie/{id}", "DELETE", controllers.HobbieController.Delete)

	// References routes
	app.addRestrictedRoute("references", "GET", controllers.ReferenceController.Read)
	app.addRestrictedRoute("reference", "POST", controllers.ReferenceController.Create)
	app.addRestrictedRoute("reference/{id}", "PUT", controllers.ReferenceController.Update)
	app.addRestrictedRoute("reference/{id}", "DELETE", controllers.ReferenceController.Delete)
}

func (app *App) addRestrictedRoute(path, method string, f func(http.ResponseWriter, *http.Request)) {
	app.Router.Handle(fmt.Sprintf("/%s", path), utils.JWT.JWTMiddlewareHandler(f)).Methods(method)
}

func (app *App) addUnRestrictedRoute(path, method string, f func(http.ResponseWriter, *http.Request)) {
	app.Router.HandleFunc(fmt.Sprintf("/%s", path), f).Methods(method)
}
