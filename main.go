package main

import (
	"os"
	"resume-api/app"
	"resume-api/utils"
)

func main() {
	// Create an object for the database information
	databaseInfo := &utils.DatabaseInfo{
		Username: "admin",
		Password: "test",
		Host:     "",
		Port:     41128,
		DBName:   "mycv-app-db",
	}
	// Create a new instance of the application
	app := app.App{}
	// Initialize the databse
	utils.Database.InitializeDatabase(databaseInfo)
	defer utils.Database.Session.Close()
	// Run the application
	address := ":" + os.Getenv("PORT")
	app.RunApplication(address)
}
