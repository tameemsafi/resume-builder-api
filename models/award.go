package models

import (
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var AwardModel = new(awardModel)

type awardModel struct {
}

type NewAwardData struct {
	Institution string `json:"institution" valid:"required~Institution name is required,length(1|100)~Institution name can be maximum of 100 characters long"`
	Title       string `json:"title" valid:"required~Title is required,length(1|100)~Title can be maximum of 100 characters long"`
	Website     string `json:"website" valid:"url~Website must be valid url"`
	Month       string `json:"month" valid:"required~Month is required,matches(^[ A-Za-z]*$)~Month can only contain letters,length(1|10)~Month can be maximum of 10 characters long"`
	Year        int    `json:"year" valid:"required~Year is required"`
	Summary     string `json:"summary" valid:"required~Award summary is required,length(1|500)~Award summary can be maximum of 500 characters long"`
}

type Award struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	UserID      bson.ObjectId `json:"userID" bson:"userID"`
	Institution string        `json:"institution" bson:"institution"`
	Title       string        `json:"title" bson:"title"`
	Website     string        `json:"website" bson:"website"`
	Month       string        `json:"startMonth" bson:"month"`
	Year        int           `json:"startYear" bson:"year"`
	Summary     string        `json:"summary" bson:"summary"`
	CreatedAt   time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt   time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const awardCollectionName = "awards"

// Create new award item in the database with a unique id
func (m *awardModel) CreateAwardItem(userID string, newAwardData *NewAwardData) (Award, error) {
	// Temp variable for new work history id
	var newAward Award
	err := m.ValidateNewAward(newAwardData)
	if err != nil {
		return newAward, err
	}
	// Create new award item for user
	newAwardID := bson.NewObjectId()
	newAward = Award{
		ID:          newAwardID,
		UserID:      bson.ObjectIdHex(userID),
		Institution: newAwardData.Institution,
		Title:       newAwardData.Title,
		Website:     newAwardData.Website,
		Month:       newAwardData.Month,
		Year:        newAwardData.Year,
		Summary:     newAwardData.Summary,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	// Write award item to mongodb
	if err := utils.Database.DB.C(awardCollectionName).Insert(newAward); err != nil {
		// Error adding new award item to database
		return newAward, err
	}
	// New award item created successfully
	return newAward, nil
}

func (m *awardModel) ValidateNewAward(newAwardData *NewAwardData) error {
	// Validate data for new award item
	_, err := govalidator.ValidateStruct(newAwardData)
	if err != nil {
		return err
	}
	return nil
}

func (m *awardModel) FindAwardItemByData(data bson.M) (Award, error) {
	var foundAward Award
	err := utils.Database.DB.C(awardCollectionName).Find(data).One(&foundAward)
	if err != nil {
		return foundAward, err
	}
	return foundAward, nil
}

func (m *awardModel) FindAllAwardItemsByUserID(userID string) ([]Award, error) {
	var foundAwards []Award
	err := utils.Database.DB.C(awardCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundAwards)
	if err != nil {
		return nil, err
	}
	return foundAwards, nil
}

func (m *awardModel) UpdateAwardByID(awardID string, newData *NewAwardData) error {
	// Validate new data
	err := m.ValidateNewAward(newData)
	if err != nil {
		return err
	}
	// Update work history with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(awardID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"title":       newData.Title,
			"institution": newData.Institution,
			"website":     newData.Website,
			"month":       newData.Month,
			"year":        newData.Year,
			"summary":     newData.Summary,
			"updatedAt":   time.Now(),
		},
	}
	err = utils.Database.DB.C(awardCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *awardModel) RemoveAwardsByID(awardID string) error {
	err := utils.Database.DB.C(awardCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(awardID),
	})
	return err
}
