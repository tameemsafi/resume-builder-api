package models

import (
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var ReferenceModel = new(referenceModel)

type referenceModel struct {
}

type NewReferenceData struct {
	Name        string `json:"name" valid:"required~Referee name is required,length(1|100)~Referee name can be maximum of 100 characters long"`
	Company     string `json:"company" valid:"required~Referee company name is required,length(1|100)~Referee compnay name can be maximum of 100 characters long"`
	Email       string `json:"email" valid:"required~Referee email is required,email~Referee email is invalid"`
	PhoneNumber string `json:"phone" valid:"numeric~Phone number can only contain numbers,length(1|20)~Phone number can be between 1 and 20 characters long"`
}

type Reference struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	UserID      bson.ObjectId `json:"userID" bson:"userID"`
	Name        string        `json:"name" bson:"name"`
	Company     string        `json:"company" bson:"company"`
	Email       string        `json:"email" bson:"email"`
	PhoneNumber string        `json:"phoneNumber" bson:"phoneNumber"`
	CreatedAt   time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt   time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const referenceCollectionName = "references"

// Create new item in the database with a unique id
func (m *referenceModel) CreateItem(userID string, newData *NewReferenceData) (Reference, error) {
	// Temp variable for new item
	var newItem Reference
	err := m.ValidateItem(newData)
	if err != nil {
		return newItem, err
	}
	// Create new item for user
	newID := bson.NewObjectId()
	newItem = Reference{
		ID:          newID,
		UserID:      bson.ObjectIdHex(userID),
		Name:        newData.Name,
		Company:     newData.Company,
		Email:       newData.Email,
		PhoneNumber: newData.PhoneNumber,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	// Write item to mongodb
	if err := utils.Database.DB.C(referenceCollectionName).Insert(newItem); err != nil {
		// Error adding new item to database
		return newItem, err
	}
	// New item created successfully
	return newItem, nil
}

func (m *referenceModel) ValidateItem(newData *NewReferenceData) error {
	// Validate data for new item
	_, err := govalidator.ValidateStruct(newData)
	if err != nil {
		return err
	}
	return nil
}

func (m *referenceModel) FindItemByData(data bson.M) (Reference, error) {
	var foundItem Reference
	err := utils.Database.DB.C(referenceCollectionName).Find(data).One(&foundItem)
	if err != nil {
		return foundItem, err
	}
	return foundItem, nil
}

func (m *referenceModel) FindAllByUserID(userID string) ([]Reference, error) {
	var foundItems []Reference
	err := utils.Database.DB.C(referenceCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundItems)
	if err != nil {
		return nil, err
	}
	return foundItems, nil
}

func (m *referenceModel) UpdateByID(ID string, newData *NewReferenceData) error {
	// Validate new data
	err := m.ValidateItem(newData)
	if err != nil {
		return err
	}
	// Update item with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(ID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"name":        newData.Name,
			"company":     newData.Company,
			"email":       newData.Email,
			"phoneNumber": newData.PhoneNumber,
			"updatedAt":   time.Now(),
		},
	}
	err = utils.Database.DB.C(referenceCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *referenceModel) RemoveItemByID(ID string) error {
	err := utils.Database.DB.C(referenceCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(ID),
	})
	return err
}
