package models

import (
	"errors"
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var WorkHistoryModel = new(workHistoryModel)

type workHistoryModel struct {
}

type NewWorkHistoryData struct {
	JobTitle   string `json:"jobTitle" valid:"required~Job title is required,length(1|80)~Job title can be maximum of 80 characters long"`
	Employer   string `json:"employer" valid:"required~Employer name is required,length(1|80)~Employer name can be maximum of 80 characters long"`
	Location   string `json:"location" valid:"required~Job location is required,length(1|100)~Job location can be maximum of 100 characters long"`
	StartMonth string `json:"startMonth" valid:"required~Start month is required,matches(^[ A-Za-z]*$)~Start month can only contain letters,length(1|10)~Start month can be maximum of 10 characters long"`
	StartYear  int    `json:"startYear" valid:"required~Start year is required"`
	EndMonth   string `json:"endMonth" valid:"matches(^[ A-Za-z]*$)~End month can only contain letters,length(1|10)~End month can be maximum of 10 characters long"`
	EndYear    int    `json:"endYear"`
	Ended      bool   `json:"ended"`
	Summary    string `json:"summary" valid:"required~Job summary is required,length(1|500)~Job summary can be maximum of 500 characters long"`
}

type WorkHistory struct {
	ID         bson.ObjectId `json:"id" bson:"_id"`
	UserID     bson.ObjectId `json:"userID" bson:"userID"`
	JobTitle   string        `json:"jobTitle" bson:"jobTitle"`
	Employer   string        `json:"employer" bson:"employer"`
	Location   string        `json:"location" bson:"location"`
	StartMonth string        `json:"startMonth" bson:"startMonth"`
	StartYear  int           `json:"startYear" bson:"startYear"`
	EndMonth   string        `json:"endMonth" bson:"endMonth"`
	EndYear    int           `json:"endYear" bson:"endYear"`
	Ended      bool          `json:"ended" bson:"ended"`
	Summary    string        `json:"summary" bson:"summary"`
	CreatedAt  time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt  time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const workHistoryCollectionName = "workHistories"

// Create new work history in the database with a unique id
func (m *workHistoryModel) CreateWorkHistory(userID string, newWorkHistoryData *NewWorkHistoryData) (WorkHistory, error) {
	// Temp variable for new work history id
	var newWorkHistory WorkHistory
	err := m.ValidateNewWorkHistoryData(newWorkHistoryData)
	if err != nil {
		return newWorkHistory, err
	}
	// Create new work history for user
	newWorkHistoryID := bson.NewObjectId()
	newWorkHistory = WorkHistory{
		ID:         newWorkHistoryID,
		UserID:     bson.ObjectIdHex(userID),
		JobTitle:   newWorkHistoryData.JobTitle,
		Employer:   newWorkHistoryData.Employer,
		Location:   newWorkHistoryData.Location,
		StartMonth: newWorkHistoryData.StartMonth,
		StartYear:  newWorkHistoryData.StartYear,
		EndMonth:   newWorkHistoryData.EndMonth,
		EndYear:    newWorkHistoryData.EndYear,
		Ended:      newWorkHistoryData.Ended,
		Summary:    newWorkHistoryData.Summary,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}
	// Write work history to mongodb
	if err := utils.Database.DB.C(workHistoryCollectionName).Insert(newWorkHistory); err != nil {
		// Error adding work history to database
		return newWorkHistory, err
	}
	// New work history created successfully
	return newWorkHistory, nil
}

func (m *workHistoryModel) ValidateNewWorkHistoryData(newWorkHistoryData *NewWorkHistoryData) error {
	// Validate data for new work history
	_, err := govalidator.ValidateStruct(newWorkHistoryData)
	if err != nil {
		return err
	}
	// Check if job has ended (if so require end month/year)
	if newWorkHistoryData.Ended {
		var workHistoryErrors govalidator.Errors
		if newWorkHistoryData.EndMonth == "" {
			workHistoryErrors = append(workHistoryErrors, govalidator.Error{Name: "endMonth", Err: errors.New("End month is required")})
		}
		if newWorkHistoryData.EndYear == 0 {
			workHistoryErrors = append(workHistoryErrors, govalidator.Error{Name: "endYear", Err: errors.New("End year is required")})
		}
		if workHistoryErrors != nil {
			return workHistoryErrors
		}
	}
	return nil
}

func (m *workHistoryModel) FindWorkHistoryByData(data bson.M) (WorkHistory, error) {
	var foundWorkHistory WorkHistory
	err := utils.Database.DB.C(workHistoryCollectionName).Find(data).One(&foundWorkHistory)
	if err != nil {
		return foundWorkHistory, err
	}
	return foundWorkHistory, nil
}

func (m *workHistoryModel) FindAllWorkHistoriesByUserID(userID string) ([]WorkHistory, error) {
	var foundWorkHistories []WorkHistory
	err := utils.Database.DB.C(workHistoryCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundWorkHistories)
	if err != nil {
		return nil, err
	}
	return foundWorkHistories, nil
}

func (m *workHistoryModel) UpdateWorkHistoryByID(workHistoryID string, newData *NewWorkHistoryData) error {
	// Validate new data
	err := m.ValidateNewWorkHistoryData(newData)
	if err != nil {
		return err
	}
	// Update work history with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(workHistoryID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"jobTitle":   newData.JobTitle,
			"employer":   newData.Employer,
			"location":   newData.Location,
			"startMonth": newData.StartMonth,
			"startYear":  newData.StartYear,
			"endMonth":   newData.EndMonth,
			"endYear":    newData.EndYear,
			"ended":      newData.Ended,
			"summary":    newData.Summary,
			"updatedAt":  time.Now(),
		},
	}
	err = utils.Database.DB.C(workHistoryCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *workHistoryModel) RemoveWorkHistoryByID(workHistoryID string) error {
	err := utils.Database.DB.C(workHistoryCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(workHistoryID),
	})
	return err
}
