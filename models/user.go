package models

import (
	"errors"
	"resume-api/utils"

	"github.com/asaskevich/govalidator"
	jwt "github.com/dgrijalva/jwt-go"

	"time"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
)

var UserModel = new(userModel)

type userModel struct {
}

// User structure for a user in the database with validation
type NewUser struct {
	FullName             string `json:"fullName" valid:"required~Fullname is required,length(1|60)~Fullname can be maximum of 60 characters long"`
	JobTitle             string `json:"jobTitle" valid:"required~Job title is required,length(1|100)~Job title can be maximum of 100 characters long"`
	CountryCode          string `json:"countryCode" valid:"ISO3166Alpha2~Country code can only be iso alpha-2 characters,required~Country code is required"`
	Username             string `json:"username" valid:"alphanum~Username can only contain letters and numbers,required~Username is required,length(1|20)~Username can be maximum of 20 characters long"`
	Email                string `json:"email" valid:"email~Email address is invalid,required~Email address is required" bson:"email"`
	Password             string `json:"password" valid:"matches(^[A-Za-z0-9_@./#&+-]*$)~Password is invalid,required~Password is required,length(5|20)~Password must be between 5 and 20 characters long"`
	PasswordConfirmation string `json:"passwordConfirmation" valid:"matches(^[A-Za-z0-9_@./#&+-]*$)~Password confirmation is invalid,required~Password confirmation is required,length(5|20)~Password confirmation must be between 5 and 20 characters long"`
}

type User struct {
	ID           bson.ObjectId `json:"id" bson:"_id"`
	ProfileImage string        `json:"profileImage" bson:"profileImage"`
	FullName     string        `json:"fullName" bson:"fullName"`
	PhoneNumber  string        `json:"phoneNumber" bson:"phoneNumber"`
	JobTitle     string        `json:"jobTitle" bson:"jobTitle"`
	CountryCode  string        `json:"countryCode" bson:"countryCode"`
	Summary      string        `json:"summary" bson:"summary"`
	Username     string        `json:"username" bson:"username"`
	Email        string        `json:"email" bson:"email"`
	PasswordHash string        `json:"passwordHash" bson:"passwordHash"`
	CreatedAt    time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt    time.Time     `json:"updatedAt" bson:"updatedAt"`
	Admin        bool          `json:"admin" bson:"admin"`
}

const usersCollecitonName string = "users"

// Create new user in the database with a unique id and hashed password
func (m *userModel) Create(newUser *NewUser) (bson.ObjectId, error) {
	// Temp variable for new user id
	var newUserID bson.ObjectId
	// Validate data for user register form
	_, err := govalidator.ValidateStruct(newUser)
	if err != nil {
		return newUserID, err
	}
	// Check if password and confirmation match
	if newUser.Password != newUser.PasswordConfirmation {
		var passErrors govalidator.Errors
		passErrors = append(passErrors, govalidator.Error{Name: "password", Err: errors.New("Password and confirmation must match")})
		passErrors = append(passErrors, govalidator.Error{Name: "passwordConfirmation", Err: errors.New("Password and confirmation must match")})
		return newUserID, passErrors
	}
	// Set admin status of new user to false
	// Hash new user password
	hash, err := bcrypt.GenerateFromPassword([]byte(newUser.PasswordConfirmation), bcrypt.DefaultCost)
	if err != nil {
		return newUserID, errors.New("Could not create password hash")
	}
	// Create new user
	newUserID = bson.NewObjectId()
	user := User{
		ID:           newUserID,
		FullName:     newUser.FullName,
		JobTitle:     newUser.JobTitle,
		CountryCode:  newUser.CountryCode,
		Username:     newUser.Username,
		Email:        newUser.Email,
		PasswordHash: string(hash),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Admin:        false,
	}
	// Write user to mongodb
	if err := utils.Database.DB.C(usersCollecitonName).Insert(user); err != nil {
		// Error adding new user to database
		return newUserID, err
	}
	// New user created successfully
	return newUserID, nil
}

func (m *userModel) FindUserByKey(key, username string) (User, error) {
	var foundUser User
	err := utils.Database.DB.C(usersCollecitonName).Find(bson.M{key: username}).One(&foundUser)
	if err != nil {
		return foundUser, err
	}
	return foundUser, nil
}

func (m *userModel) FindUserByID(id string) (User, error) {
	var foundUser User
	err := utils.Database.DB.C(usersCollecitonName).Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&foundUser)
	if err != nil {
		return foundUser, err
	}
	return foundUser, nil
}

func (m *userModel) FindUserByUsernameOrEmail(usernameOrEmail string) (User, error) {
	var foundUser User
	err := utils.Database.DB.C(usersCollecitonName).Find(bson.M{
		"$or": []interface{}{
			bson.M{"username": usernameOrEmail},
			bson.M{"email": usernameOrEmail},
		},
	}).One(&foundUser)
	if err != nil {
		return foundUser, err
	}
	return foundUser, nil
}

func (m *userModel) FindUserByRequestContextValue(user interface{}) (User, error) {
	var foundUser User
	userClaims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	userID := userClaims["user_id"]
	if userID == nil {
		return foundUser, errors.New("No user ID found")
	}
	// Check if user exists
	foundUser, err := m.FindUserByID(userID.(string))
	if err != nil {
		return foundUser, errors.New("Could not find user")
	}
	return foundUser, nil
}

func (m *userModel) UpdateUserKey(userID string, profileImageName string) error {
	findQuery := bson.M{"_id": bson.ObjectIdHex(userID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"profileImage": profileImageName,
			"updatedAt":    time.Now(),
		},
	}
	err := utils.Database.DB.C(usersCollecitonName).Update(findQuery, updateQuery)
	return err
}

func (m *userModel) UpdateUserData(userID string, data bson.M) error {
	findQuery := bson.M{"_id": bson.ObjectIdHex(userID)}
	updateQuery := bson.M{
		"$set": data,
	}
	err := utils.Database.DB.C(usersCollecitonName).Update(findQuery, updateQuery)
	return err
}
