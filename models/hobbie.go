package models

import (
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var HobbieModel = new(hobbieModel)

type hobbieModel struct {
}

type NewHobbieData struct {
	Title string `json:"title" valid:"required~Hobbie title is required,length(1|100)~Hobbie title can be maximum of 100 characters long"`
}

type Hobbie struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	UserID    bson.ObjectId `json:"userID" bson:"userID"`
	Title     string        `json:"title" bson:"title"`
	CreatedAt time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const hobbieCollectionName = "hobbies"

// Create new item in the database with a unique id
func (m *hobbieModel) CreateItem(userID string, newData *NewHobbieData) (Hobbie, error) {
	// Temp variable for new item
	var newItem Hobbie
	err := m.ValidateItem(newData)
	if err != nil {
		return newItem, err
	}
	// Create new item for user
	newID := bson.NewObjectId()
	newItem = Hobbie{
		ID:        newID,
		UserID:    bson.ObjectIdHex(userID),
		Title:     newData.Title,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	// Write item to mongodb
	if err := utils.Database.DB.C(hobbieCollectionName).Insert(newItem); err != nil {
		// Error adding new item to database
		return newItem, err
	}
	// New item created successfully
	return newItem, nil
}

func (m *hobbieModel) ValidateItem(newData *NewHobbieData) error {
	// Validate data for new item
	_, err := govalidator.ValidateStruct(newData)
	if err != nil {
		return err
	}
	return nil
}

func (m *hobbieModel) FindItemByData(data bson.M) (Hobbie, error) {
	var foundItem Hobbie
	err := utils.Database.DB.C(hobbieCollectionName).Find(data).One(&foundItem)
	if err != nil {
		return foundItem, err
	}
	return foundItem, nil
}

func (m *hobbieModel) FindAllByUserID(userID string) ([]Hobbie, error) {
	var foundItems []Hobbie
	err := utils.Database.DB.C(hobbieCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundItems)
	if err != nil {
		return nil, err
	}
	return foundItems, nil
}

func (m *hobbieModel) UpdateByID(ID string, newData *NewHobbieData) error {
	// Validate new data
	err := m.ValidateItem(newData)
	if err != nil {
		return err
	}
	// Update item with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(ID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"title":     newData.Title,
			"updatedAt": time.Now(),
		},
	}
	err = utils.Database.DB.C(hobbieCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *hobbieModel) RemoveItemByID(ID string) error {
	err := utils.Database.DB.C(hobbieCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(ID),
	})
	return err
}
