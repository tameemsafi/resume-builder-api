package models

import (
	"errors"
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var EducationModel = new(educationModel)

type educationModel struct {
}

type NewEducationData struct {
	Institution string `json:"institution" valid:"required~Institution name is required,length(1|100)~Institution name can be maximum of 100 characters long"`
	Location    string `json:"location" valid:"required~Institution location is required,length(1|100)~Institution location can be maximum of 100 characters long"`
	StartMonth  string `json:"startMonth" valid:"required~Start month is required,matches(^[ A-Za-z]*$)~Start month can only contain letters,length(1|10)~Start month can be maximum of 10 characters long"`
	StartYear   int    `json:"startYear" valid:"required~Start year is required"`
	EndMonth    string `json:"endMonth" valid:"matches(^[ A-Za-z]*$)~End month can only contain letters,length(1|10)~End month can be maximum of 10 characters long"`
	EndYear     int    `json:"endYear"`
	Ended       bool   `json:"ended"`
	Summary     string `json:"summary" valid:"required~Education summary is required,length(1|500)~Education summary can be maximum of 500 characters long"`
}

type Education struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	UserID      bson.ObjectId `json:"userID" bson:"userID"`
	Institution string        `json:"institution" bson:"institution"`
	Location    string        `json:"location" bson:"location"`
	StartMonth  string        `json:"startMonth" bson:"startMonth"`
	StartYear   int           `json:"startYear" bson:"startYear"`
	EndMonth    string        `json:"endMonth" bson:"endMonth"`
	EndYear     int           `json:"endYear" bson:"endYear"`
	Ended       bool          `json:"ended" bson:"ended"`
	Summary     string        `json:"summary" bson:"summary"`
	CreatedAt   time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt   time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const educationCollectionName = "educations"

// Create new education item in the database with a unique id
func (m *educationModel) CreateEducationItem(userID string, newEducationData *NewEducationData) (Education, error) {
	// Temp variable for new work history id
	var newEducation Education
	err := m.ValidateNewEducationData(newEducationData)
	if err != nil {
		return newEducation, err
	}
	// Create new education item for user
	newEducationID := bson.NewObjectId()
	newEducation = Education{
		ID:          newEducationID,
		UserID:      bson.ObjectIdHex(userID),
		Institution: newEducationData.Institution,
		Location:    newEducationData.Location,
		StartMonth:  newEducationData.StartMonth,
		StartYear:   newEducationData.StartYear,
		EndMonth:    newEducationData.EndMonth,
		EndYear:     newEducationData.EndYear,
		Ended:       newEducationData.Ended,
		Summary:     newEducationData.Summary,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	// Write education item to mongodb
	if err := utils.Database.DB.C(educationCollectionName).Insert(newEducation); err != nil {
		// Error adding new education item to database
		return newEducation, err
	}
	// New education item created successfully
	return newEducation, nil
}

func (m *educationModel) ValidateNewEducationData(newEducationData *NewEducationData) error {
	// Validate data for new work history
	_, err := govalidator.ValidateStruct(newEducationData)
	if err != nil {
		return err
	}
	// Check if education has ended (if so require end month/year)
	if newEducationData.Ended {
		var educationErrors govalidator.Errors
		if newEducationData.EndMonth == "" {
			educationErrors = append(educationErrors, govalidator.Error{Name: "endMonth", Err: errors.New("End month is required")})
		}
		if newEducationData.EndYear == 0 {
			educationErrors = append(educationErrors, govalidator.Error{Name: "endYear", Err: errors.New("End year is required")})
		}
		if educationErrors != nil {
			return educationErrors
		}
	}
	return nil
}

func (m *educationModel) FindEducationItemByData(data bson.M) (Education, error) {
	var foundEducation Education
	err := utils.Database.DB.C(educationCollectionName).Find(data).One(&foundEducation)
	if err != nil {
		return foundEducation, err
	}
	return foundEducation, nil
}

func (m *educationModel) FindAllEducationItemsByUserID(userID string) ([]Education, error) {
	var foundEducations []Education
	err := utils.Database.DB.C(educationCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundEducations)
	if err != nil {
		return nil, err
	}
	return foundEducations, nil
}

func (m *educationModel) UpdateEducationByID(educationID string, newData *NewEducationData) error {
	// Validate new data
	err := m.ValidateNewEducationData(newData)
	if err != nil {
		return err
	}
	// Update work history with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(educationID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"institution": newData.Institution,
			"location":    newData.Location,
			"startMonth":  newData.StartMonth,
			"startYear":   newData.StartYear,
			"endMonth":    newData.EndMonth,
			"endYear":     newData.EndYear,
			"ended":       newData.Ended,
			"summary":     newData.Summary,
			"updatedAt":   time.Now(),
		},
	}
	err = utils.Database.DB.C(educationCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *educationModel) RemoveEducationByID(educationID string) error {
	err := utils.Database.DB.C(educationCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(educationID),
	})
	return err
}
