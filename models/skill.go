package models

import (
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var SkillModel = new(skillModel)

type skillModel struct {
}

type NewSkillData struct {
	Title string `json:"title" valid:"required~Skill title is required,length(1|100)~Skill title can be maximum of 100 characters long"`
	Level string `json:"level" valid:"required~Skill level is required,length(1|15)~Skill level can be maximum of 15 characters long"`
}

type Skill struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	UserID    bson.ObjectId `json:"userID" bson:"userID"`
	Title     string        `json:"title" bson:"title"`
	Level     string        `json:"level" bson:"level"`
	CreatedAt time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const skillCollectionName = "skills"

// Create new item in the database with a unique id
func (m *skillModel) CreateSkillItem(userID string, newSkillData *NewSkillData) (Skill, error) {
	// Temp variable for new item
	var newSkill Skill
	err := m.ValidateNewSkill(newSkillData)
	if err != nil {
		return newSkill, err
	}
	// Create new item for user
	newSkillID := bson.NewObjectId()
	newSkill = Skill{
		ID:        newSkillID,
		UserID:    bson.ObjectIdHex(userID),
		Title:     newSkillData.Title,
		Level:     newSkillData.Level,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	// Write item to mongodb
	if err := utils.Database.DB.C(skillCollectionName).Insert(newSkill); err != nil {
		// Error adding new item to database
		return newSkill, err
	}
	// New item created successfully
	return newSkill, nil
}

func (m *skillModel) ValidateNewSkill(newSkillData *NewSkillData) error {
	// Validate data for new item
	_, err := govalidator.ValidateStruct(newSkillData)
	if err != nil {
		return err
	}
	return nil
}

func (m *skillModel) FindSkillItemByData(data bson.M) (Skill, error) {
	var foundSkill Skill
	err := utils.Database.DB.C(skillCollectionName).Find(data).One(&foundSkill)
	if err != nil {
		return foundSkill, err
	}
	return foundSkill, nil
}

func (m *skillModel) FindAllSkillItemsByUserID(userID string) ([]Skill, error) {
	var foundSkills []Skill
	err := utils.Database.DB.C(skillCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundSkills)
	if err != nil {
		return nil, err
	}
	return foundSkills, nil
}

func (m *skillModel) UpdateSkillByID(skillID string, newData *NewSkillData) error {
	// Validate new data
	err := m.ValidateNewSkill(newData)
	if err != nil {
		return err
	}
	// Update item with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(skillID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"title":     newData.Title,
			"level":     newData.Level,
			"updatedAt": time.Now(),
		},
	}
	err = utils.Database.DB.C(skillCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *skillModel) RemoveSkillByID(skillID string) error {
	err := utils.Database.DB.C(skillCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(skillID),
	})
	return err
}
