package models

import (
	"resume-api/utils"
	"time"

	"github.com/asaskevich/govalidator"

	"gopkg.in/mgo.v2/bson"
)

var LanguageModel = new(languageModel)

type languageModel struct {
}

type NewLanguageData struct {
	Language string `json:"language" valid:"required~Language name is required,length(1|100)~Language name can be maximum of 100 characters long"`
	Level    string `json:"level" valid:"required~Level is required,length(1|15)~Level can be maximum of 15 characters long"`
}

type Language struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	UserID    bson.ObjectId `json:"userID" bson:"userID"`
	Language  string        `json:"language" bson:"language"`
	Level     string        `json:"level" bson:"level"`
	CreatedAt time.Time     `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time     `json:"updatedAt" bson:"updatedAt"`
}

const languageCollectionName = "languages"

// Create new item in the database with a unique id
func (m *languageModel) CreateLanguageItem(userID string, newData *NewLanguageData) (Language, error) {
	// Temp variable for new item
	var newItem Language
	err := m.ValidateNewLanguage(newData)
	if err != nil {
		return newItem, err
	}
	// Create new item for user
	newID := bson.NewObjectId()
	newItem = Language{
		ID:        newID,
		UserID:    bson.ObjectIdHex(userID),
		Language:  newData.Language,
		Level:     newData.Level,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	// Write item to mongodb
	if err := utils.Database.DB.C(languageCollectionName).Insert(newItem); err != nil {
		// Error adding new item to database
		return newItem, err
	}
	// New item created successfully
	return newItem, nil
}

func (m *languageModel) ValidateNewLanguage(newData *NewLanguageData) error {
	// Validate data for new item
	_, err := govalidator.ValidateStruct(newData)
	if err != nil {
		return err
	}
	return nil
}

func (m *languageModel) FindLanguageItemByData(data bson.M) (Language, error) {
	var foundItem Language
	err := utils.Database.DB.C(languageCollectionName).Find(data).One(&foundItem)
	if err != nil {
		return foundItem, err
	}
	return foundItem, nil
}

func (m *languageModel) FindAllLanguageItemsByUserID(userID string) ([]Language, error) {
	var foundItems []Language
	err := utils.Database.DB.C(languageCollectionName).Find(bson.M{"userID": bson.ObjectIdHex(userID)}).All(&foundItems)
	if err != nil {
		return nil, err
	}
	return foundItems, nil
}

func (m *languageModel) UpdateLanguageByID(ID string, newData *NewLanguageData) error {
	// Validate new data
	err := m.ValidateNewLanguage(newData)
	if err != nil {
		return err
	}
	// Update item with new data
	findQuery := bson.M{"_id": bson.ObjectIdHex(ID)}
	updateQuery := bson.M{
		"$set": bson.M{
			"language":  newData.Language,
			"level":     newData.Level,
			"updatedAt": time.Now(),
		},
	}
	err = utils.Database.DB.C(languageCollectionName).Update(findQuery, updateQuery)
	return err
}

func (m *languageModel) RemoveLanguageByID(ID string) error {
	err := utils.Database.DB.C(languageCollectionName).Remove(bson.M{
		"_id": bson.ObjectIdHex(ID),
	})
	return err
}
