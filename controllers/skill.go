package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var SkillController = new(skillController)

type skillController struct {
}

type SkillItemsResponse struct {
	SkillItems []models.Skill `json:"skillItems"`
	StatusCode int            `json:"statusCode"`
}

var model = models.SkillModel
var createNew = model.CreateSkillItem
var findAll = model.FindAllSkillItemsByUserID
var findOne = model.FindSkillItemByData
var updateWithID = model.UpdateSkillByID
var removeWithID = model.RemoveSkillByID
var itemName = "Skill"

func (c *skillController) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newItem := models.NewSkillData{}
	if err := json.NewDecoder(r.Body).Decode(&newItem); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create an item in database for user
	if _, err := createNew(user.ID.Hex(), &newItem); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Skill item created", http.StatusCreated)
}

func (c *skillController) Read(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find all items for user
	foundItems, err := findAll(user.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send response with data
	response := SkillItemsResponse{
		SkillItems: foundItems,
		StatusCode: http.StatusOK,
	}
	utils.Response.SendJSON(w, response, http.StatusOK)
}

func (c *skillController) Update(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newData := models.NewSkillData{}
	if err := json.NewDecoder(r.Body).Decode(&newData); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find item by id from route
	routeVars := mux.Vars(r)
	itemID := routeVars["id"]
	foundItem, err := findOne(bson.M{
		"_id":    bson.ObjectIdHex(itemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find education item", http.StatusBadRequest)
		return
	}
	// Update item with new data
	if err := updateWithID(foundItem.ID.Hex(), &newData); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, itemName+" item updated", http.StatusOK)
}

func (c *skillController) Delete(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find item by id from route
	routeVars := mux.Vars(r)
	itemID := routeVars["id"]
	foundItem, err := findOne(bson.M{
		"_id":    bson.ObjectIdHex(itemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find "+itemName+" item", http.StatusBadRequest)
		return
	}
	// Delete  item
	err = removeWithID(foundItem.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Could not delete "+itemName+" item", http.StatusBadRequest)
		return
	}
	utils.Response.SendMessage(w, itemName+" item deleted", http.StatusOK)
}
