package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var EducationController = new(educationController)

type educationController struct {
}

type EducationItemsResponse struct {
	EducationItems []models.Education `json:"educationItems"`
	StatusCode     int                `json:"statusCode"`
}

func (c *educationController) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newEducation := models.NewEducationData{}
	if err := json.NewDecoder(r.Body).Decode(&newEducation); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create an education item in database for user
	if _, err := models.EducationModel.CreateEducationItem(user.ID.Hex(), &newEducation); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Education item created", http.StatusCreated)
}

func (c *educationController) Read(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find all education items for user
	foundEducations, err := models.EducationModel.FindAllEducationItemsByUserID(user.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send response with data
	educationsItemsResponse := EducationItemsResponse{
		EducationItems: foundEducations,
		StatusCode:     http.StatusOK,
	}
	utils.Response.SendJSON(w, educationsItemsResponse, http.StatusOK)
}

func (c *educationController) Update(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newData := models.NewEducationData{}
	if err := json.NewDecoder(r.Body).Decode(&newData); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find education item  by id from route
	routeVars := mux.Vars(r)
	educationID := routeVars["id"]
	foundEducationItem, err := models.EducationModel.FindEducationItemByData(bson.M{
		"_id":    bson.ObjectIdHex(educationID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find education item", http.StatusBadRequest)
		return
	}
	// Update education item with new data
	if err := models.EducationModel.UpdateEducationByID(foundEducationItem.ID.Hex(), &newData); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Education item updated", http.StatusOK)
}

func (c *educationController) Delete(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find work history by id from route
	routeVars := mux.Vars(r)
	educationItemID := routeVars["id"]
	foundEducationItem, err := models.EducationModel.FindEducationItemByData(bson.M{
		"_id":    bson.ObjectIdHex(educationItemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find education item", http.StatusBadRequest)
		return
	}
	// Delete work history
	err = models.EducationModel.RemoveEducationByID(foundEducationItem.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Could not delete education item", http.StatusBadRequest)
		return
	}
	utils.Response.SendMessage(w, "Education item deleted", http.StatusOK)
}
