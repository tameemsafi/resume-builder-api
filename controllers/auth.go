package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"gopkg.in/mgo.v2/bson"

	"time"

	"github.com/asaskevich/govalidator"
	jwt "github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

// AuthController instance of the auth controller
var AuthController = new(auth)

type auth struct {
}

type loginData struct {
	Username string `json:"username" valid:"required~Username is required"`
	Password string `json:"password" valid:"required~Password is required,matches(^[A-Za-z0-9_@./#&+-]*$)~Password is invalid"`
}

func (c *auth) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newUser := models.NewUser{}
	if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Check username is unique
	usernameCount, err := utils.Database.DB.C("users").Find(bson.M{"username": newUser.Username}).Count()
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	if usernameCount > 0 {
		utils.Response.SendMessage(w, "Username already exists", http.StatusBadRequest)
		return
	}
	// Check email is unique
	emailCount, err := utils.Database.DB.C("users").Find(bson.M{"email": newUser.Email}).Count()
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	if emailCount > 0 {
		utils.Response.SendMessage(w, "Email address already exists", http.StatusBadRequest)
		return
	}
	// Create a user account using the user model
	if _, err := models.UserModel.Create(&newUser); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Return resposne if successful
	utils.Response.SendMessage(w, "Account created", http.StatusCreated)
}

func (c *auth) Login(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	userLoginData := loginData{}
	if err := json.NewDecoder(r.Body).Decode(&userLoginData); err != nil {
		utils.Response.SendMessage(w, "Invalid data, please send a string username/email and password", http.StatusBadRequest)
		return
	}
	// Validate login input data
	_, err := govalidator.ValidateStruct(&userLoginData)
	if err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Find user by username or email
	user, err := models.UserModel.FindUserByUsernameOrEmail(userLoginData.Username)
	if err != nil {
		utils.Response.SendMessage(w, "User does not exist", http.StatusBadRequest)
		return
	}
	// Check password matches hash
	passwordCheck := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(userLoginData.Password))
	if passwordCheck != nil {
		utils.Response.SendMessage(w, "Password is incorrect", http.StatusBadRequest)
		return
	}
	// User login data is correct
	// Generate JWT token
	signedToken, err := c.generateTokenForUser(string(user.ID))
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.Response.SendMessage(w, signedToken, http.StatusOK)
}

func (c *auth) GenerateToken(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Generate a new token for user
	signedToken, err := c.generateTokenForUser(string(user.ID))
	if err != nil {
		utils.Response.SendMessage(w, "Could not generate token", http.StatusBadRequest)
		return
	}
	// Get the user id from the claims
	utils.Response.SendMessage(w, signedToken, http.StatusOK)
}

func (c *auth) generateTokenForUser(userID string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["user_id"] = userID
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	signedToken, err := token.SignedString(utils.Config.JWTSecret())
	return signedToken, err
}
