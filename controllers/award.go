package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var AwardController = new(awardController)

type awardController struct {
}

type AwardItemsResponse struct {
	AwardItems []models.Award `json:"awardItems"`
	StatusCode int            `json:"statusCode"`
}

func (c *awardController) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newAward := models.NewAwardData{}
	if err := json.NewDecoder(r.Body).Decode(&newAward); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create an award item in database for user
	if _, err := models.AwardModel.CreateAwardItem(user.ID.Hex(), &newAward); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Award item created", http.StatusCreated)
}

func (c *awardController) Read(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find all education items for user
	foundAwards, err := models.AwardModel.FindAllAwardItemsByUserID(user.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send response with data
	awardItemsResponse := AwardItemsResponse{
		AwardItems: foundAwards,
		StatusCode: http.StatusOK,
	}
	utils.Response.SendJSON(w, awardItemsResponse, http.StatusOK)
}

func (c *awardController) Update(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newData := models.NewAwardData{}
	if err := json.NewDecoder(r.Body).Decode(&newData); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find award item  by id from route
	routeVars := mux.Vars(r)
	awardID := routeVars["id"]
	foundAwardItem, err := models.AwardModel.FindAwardItemByData(bson.M{
		"_id":    bson.ObjectIdHex(awardID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find education item", http.StatusBadRequest)
		return
	}
	// Update award item with new data
	if err := models.AwardModel.UpdateAwardByID(foundAwardItem.ID.Hex(), &newData); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Award item updated", http.StatusOK)
}

func (c *awardController) Delete(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find award item by id from route
	routeVars := mux.Vars(r)
	awardID := routeVars["id"]
	foundAwardItem, err := models.AwardModel.FindAwardItemByData(bson.M{
		"_id":    bson.ObjectIdHex(awardID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find award item", http.StatusBadRequest)
		return
	}
	// Delete award item
	err = models.AwardModel.RemoveAwardsByID(foundAwardItem.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Could not delete award item", http.StatusBadRequest)
		return
	}
	utils.Response.SendMessage(w, "Award item deleted", http.StatusOK)
}
