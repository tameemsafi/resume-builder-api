package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var HobbieController = new(hobbieController)

type hobbieController struct {
}

type HobbieItemsResponse struct {
	HobbieItems []models.Hobbie `json:"hobbieItems"`
	StatusCode  int             `json:"statusCode"`
}

func (c *hobbieController) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newItem := models.NewHobbieData{}
	if err := json.NewDecoder(r.Body).Decode(&newItem); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create an item in database for user
	if _, err := models.HobbieModel.CreateItem(user.ID.Hex(), &newItem); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Hobbie item created", http.StatusCreated)
}

func (c *hobbieController) Read(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find all items for user
	foundItems, err := models.HobbieModel.FindAllByUserID(user.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send response with data
	response := HobbieItemsResponse{
		HobbieItems: foundItems,
		StatusCode:  http.StatusOK,
	}
	utils.Response.SendJSON(w, response, http.StatusOK)
}

func (c *hobbieController) Update(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newData := models.NewHobbieData{}
	if err := json.NewDecoder(r.Body).Decode(&newData); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find item by id from route
	routeVars := mux.Vars(r)
	itemID := routeVars["id"]
	foundItem, err := models.ReferenceModel.FindItemByData(bson.M{
		"_id":    bson.ObjectIdHex(itemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find hobbie item", http.StatusBadRequest)
		return
	}
	// Update item with new data
	if err := models.HobbieModel.UpdateByID(foundItem.ID.Hex(), &newData); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Hobbie item updated", http.StatusOK)
}

func (c *hobbieController) Delete(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find item by id from route
	routeVars := mux.Vars(r)
	itemID := routeVars["id"]
	foundItem, err := models.HobbieModel.FindItemByData(bson.M{
		"_id":    bson.ObjectIdHex(itemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find hobbie item", http.StatusBadRequest)
		return
	}
	// Delete  item
	err = models.HobbieModel.RemoveItemByID(foundItem.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Could not delete hobbie item", http.StatusBadRequest)
		return
	}
	utils.Response.SendMessage(w, "Hobbie item deleted", http.StatusOK)
}
