package controllers

import (
	"bytes"
	"crypto/md5"
	"io/ioutil"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"gopkg.in/mgo.v2/bson"

	"encoding/hex"
	"encoding/json"

	"fmt"

	"github.com/asaskevich/govalidator"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

var ProfileController = new(profile)

// User structure for a user in the database with validation
type profileUpdateData struct {
	FullName    string `json:"fullName" valid:"required~Fullname is required,length(1|60)~Fullname can be maximum of 60 characters long"`
	PhoneNumber string `json:"phoneNumber" valid:"required~Phone number is required,numeric~Phone number can only contain numbers,length(1|20)~Phone number can be between 1 and 20 characters long"`
	JobTitle    string `json:"jobTitle" valid:"required~Job title is required,length(1|100)~Job title can be maximum of 100 characters long"`
	CountryCode string `json:"countryCode" valid:"ISO3166Alpha2~Country code can only be iso alpha-2 characters,required~Country code is required"`
	Summary     string `json:"summary" valid:"required~Summary is required,length(1|500)~Summary must be between 1 and 500 characters long"`
}

type profile struct {
}

func (p *profile) UploadAvatar(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Variables for image validation
	const MB = 1 << 20
	const maxSize = 2 * MB
	var allowedTypes [3]string
	allowedTypes[0] = "image/jpeg"
	allowedTypes[1] = "image/png"
	allowedTypes[2] = "image/pjpeg"
	// Parse the incoming file with a max size of 2 MB
	r.ParseMultipartForm(maxSize)
	// Get the file from the variable image
	file, _, err := r.FormFile("image")
	// Close file after request is complete
	defer file.Close()
	if err != nil {
		utils.Response.SendMessage(w, "Could not parse file", http.StatusBadRequest)
		return
	}
	// Get the file information
	fileHeader := make([]byte, 512)
	if _, err := file.Read(fileHeader); err != nil {
		utils.Response.SendMessage(w, "Could not proccess file ", http.StatusBadRequest)
		return
	}
	if _, err := file.Seek(0, 0); err != nil {
		utils.Response.SendMessage(w, "Could not proccess file", http.StatusBadRequest)
		return
	}
	// Find the mime type
	fileType := http.DetectContentType(fileHeader)
	mimeTypeAccepted := false
	for _, mimeType := range allowedTypes {
		// Check if mimetype matches the accepted types
		if fileType == mimeType {
			mimeTypeAccepted = true
		}
	}
	// Send error if mimetype does not match
	if mimeTypeAccepted == false {
		utils.Response.SendMessage(w, "Only allowed JPEG/JPG/PNG file", http.StatusBadRequest)
		return
	}
	// Upload file to s3
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		utils.Response.SendMessage(w, "Error reading file", http.StatusBadRequest)
		return
	}
	fileBytesReader := bytes.NewReader(fileBytes)
	fileSize := len(fileBytes)
	// Create a hash with the user id to store their avatar
	md5Hasher := md5.New()
	md5Hasher.Write([]byte(user.ID))
	userIDHash := hex.EncodeToString(md5Hasher.Sum(nil))
	var avatarImageName string
	if fileType == "image/png" {
		avatarImageName = userIDHash + ".png"
	} else if fileType == "image/jpeg" {
		avatarImageName = userIDHash + ".jpg"
	} else if fileType == "image/pjpeg" {
		avatarImageName = userIDHash + ".jpg"
	}
	path := fmt.Sprintf("/avatars/%v", avatarImageName)
	// Setup the paramaters used to upload image
	s3Params := &s3.PutObjectInput{
		Bucket:        aws.String("resume-api-bucket"),
		Key:           aws.String(path),
		Body:          fileBytesReader,
		ContentLength: aws.Int64(int64(fileSize)),
		ContentType:   aws.String(fileType),
	}
	_, err = utils.AWSS3.BucketInstance().PutObject(s3Params)
	if err != nil {
		// Image did not upload error
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Update user profle image
	err = models.UserModel.UpdateUserKey(user.ID.Hex(), avatarImageName)
	if err != nil {
		utils.Response.SendMessage(w, "Error updating user in database", http.StatusBadRequest)
		return
	}
	// Image was successfully uploaded
	// Send response with public link
	publicAvatarLink := fmt.Sprintf("%v/resume-api-bucket/avatars/%v", utils.AWSS3.BucketInstance().Endpoint, avatarImageName)
	utils.Response.SendMessage(w, publicAvatarLink, http.StatusOK)
}

func (p *profile) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newProfileInfo := profileUpdateData{}
	if err := json.NewDecoder(r.Body).Decode(&newProfileInfo); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Validate login input data
	_, err = govalidator.ValidateStruct(&newProfileInfo)
	if err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Update profile in database
	err = models.UserModel.UpdateUserData(user.ID.Hex(), bson.M{
		"fullName":    newProfileInfo.FullName,
		"phoneNumber": newProfileInfo.PhoneNumber,
		"jobTitle":    newProfileInfo.JobTitle,
		"countryCode": newProfileInfo.CountryCode,
		"summary":     newProfileInfo.Summary,
	})
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Profile updated", http.StatusOK)
}
