package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"gopkg.in/mgo.v2/bson"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
)

var WorkHistoryController = new(workHistory)

type workHistory struct {
}

type WorkHistoriesResponse struct {
	WorkHistories []models.WorkHistory `json:"workHistories"`
	StatusCode    int                  `json:"statusCode"`
}

func (c *workHistory) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newWorkHistory := models.NewWorkHistoryData{}
	if err := json.NewDecoder(r.Body).Decode(&newWorkHistory); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create a work history in database for user
	if _, err := models.WorkHistoryModel.CreateWorkHistory(user.ID.Hex(), &newWorkHistory); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Work history created", http.StatusCreated)
}

func (c *workHistory) Read(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find all work histories for user
	foundWorkHistories, err := models.WorkHistoryModel.FindAllWorkHistoriesByUserID(user.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send response with data
	workHistoriesResponse := WorkHistoriesResponse{
		WorkHistories: foundWorkHistories,
		StatusCode:    http.StatusOK,
	}
	utils.Response.SendJSON(w, workHistoriesResponse, http.StatusOK)
}

func (c *workHistory) Update(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newData := models.NewWorkHistoryData{}
	if err := json.NewDecoder(r.Body).Decode(&newData); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find work history by id from route
	routeVars := mux.Vars(r)
	workHistoryID := routeVars["id"]
	foundWorkHistory, err := models.WorkHistoryModel.FindWorkHistoryByData(bson.M{
		"_id":    bson.ObjectIdHex(workHistoryID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find work history", http.StatusBadRequest)
		return
	}
	// Update work history with new data
	if err := models.WorkHistoryModel.UpdateWorkHistoryByID(foundWorkHistory.ID.Hex(), &newData); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Work history updated", http.StatusOK)
}

func (c *workHistory) Delete(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find work history by id from route
	routeVars := mux.Vars(r)
	workHistoryID := routeVars["id"]
	foundWorkHistory, err := models.WorkHistoryModel.FindWorkHistoryByData(bson.M{
		"_id":    bson.ObjectIdHex(workHistoryID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find work history", http.StatusBadRequest)
		return
	}
	// Delete work history
	err = models.WorkHistoryModel.RemoveWorkHistoryByID(foundWorkHistory.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Could not delete work history", http.StatusBadRequest)
		return
	}
	utils.Response.SendMessage(w, "Work history deleted", http.StatusOK)
}
