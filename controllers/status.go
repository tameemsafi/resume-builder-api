package controllers

import (
	"net/http"
	"resume-api/utils"
)

var StatusController = new(status)

type status struct {
}

func (s *status) Status(w http.ResponseWriter, r *http.Request) {
	utils.Response.SuccessMessage(w, "API server is runing")
}
