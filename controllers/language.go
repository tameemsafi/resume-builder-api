package controllers

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"resume-api/utils"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var LanguageController = new(languageController)

type languageController struct {
}

type LanguageItemsResponse struct {
	LanguageItems []models.Language `json:"languageItems"`
	StatusCode    int               `json:"statusCode"`
}

func (c *languageController) Create(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newItem := models.NewLanguageData{}
	if err := json.NewDecoder(r.Body).Decode(&newItem); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create an item in database for user
	if _, err := models.LanguageModel.CreateLanguageItem(user.ID.Hex(), &newItem); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Language item created", http.StatusCreated)
}

func (c *languageController) Read(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find all items for user
	foundItems, err := models.LanguageModel.FindAllLanguageItemsByUserID(user.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Database error", http.StatusBadRequest)
		return
	}
	// Send response with data
	response := LanguageItemsResponse{
		LanguageItems: foundItems,
		StatusCode:    http.StatusOK,
	}
	utils.Response.SendJSON(w, response, http.StatusOK)
}

func (c *languageController) Update(w http.ResponseWriter, r *http.Request) {
	// Check to make sure there is data in the body
	if r.Body == nil {
		utils.Response.SendMessage(w, "Please send some data", http.StatusBadRequest)
		return
	}
	// Parse incoming json data
	newData := models.NewLanguageData{}
	if err := json.NewDecoder(r.Body).Decode(&newData); err != nil {
		utils.Response.SendMessage(w, "Invalid data", http.StatusBadRequest)
		return
	}
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find item by id from route
	routeVars := mux.Vars(r)
	itemID := routeVars["id"]
	foundItem, err := models.LanguageModel.FindLanguageItemByData(bson.M{
		"_id":    bson.ObjectIdHex(itemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find education item", http.StatusBadRequest)
		return
	}
	// Update item with new data
	if err := models.LanguageModel.UpdateLanguageByID(foundItem.ID.Hex(), &newData); err != nil {
		utils.Response.SendErrors(w, govalidator.ErrorsByField(err), http.StatusBadRequest)
		return
	}
	// Send success message
	utils.Response.SendMessage(w, "Language item updated", http.StatusOK)
}

func (c *languageController) Delete(w http.ResponseWriter, r *http.Request) {
	// Get authenticated user
	userContext := r.Context().Value("user")
	user, err := models.UserModel.FindUserByRequestContextValue(userContext)
	if err != nil {
		utils.Response.SendMessage(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Find item by id from route
	routeVars := mux.Vars(r)
	itemID := routeVars["id"]
	foundItem, err := models.LanguageModel.FindLanguageItemByData(bson.M{
		"_id":    bson.ObjectIdHex(itemID),
		"userID": bson.ObjectIdHex(user.ID.Hex()),
	})
	if err != nil {
		utils.Response.SendMessage(w, "Could not find language item", http.StatusBadRequest)
		return
	}
	// Delete  item
	err = models.LanguageModel.RemoveLanguageByID(foundItem.ID.Hex())
	if err != nil {
		utils.Response.SendMessage(w, "Could not delete langauge item", http.StatusBadRequest)
		return
	}
	utils.Response.SendMessage(w, "Language item deleted", http.StatusOK)
}
