package utils

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// AWSS3 amazon s3 helper functions
var AWSS3 = new(awsS3)

type awsS3 struct {
}

func (awsS3 *awsS3) BucketInstance() *s3.S3 {
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("eu-west-2"),
		Credentials: credentials.NewStaticCredentials("KEY_HERE", "KEY_HERE", ""),
	})
	if err != nil {
		panic("Could not connect to aws")
	}
	return s3.New(sess)
}
