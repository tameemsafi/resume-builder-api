package utils

import (
	"fmt"

	"gopkg.in/mgo.v2"
)

// Database object instance for later use
var Database = new(database)

type database struct {
	DB      *mgo.Database
	Session *mgo.Session
}

// DatabaseInfo structure of the mongo db databse connection
type DatabaseInfo struct {
	Username string
	Password string
	Host     string
	Port     int
	DBName   string
}

// InitializeDatabase connect to the mongo db database and store the connection
func (db *database) InitializeDatabase(databaseInfo *DatabaseInfo) {
	// Create a mongodb connection url
	var mongoURL = fmt.Sprintf(
		"mongodb://%s:%s@%s:%v/%s",
		databaseInfo.Username,
		databaseInfo.Password,
		databaseInfo.Host,
		databaseInfo.Port,
		databaseInfo.DBName,
	)
	// Try to connect to mongodb
	session, err := mgo.Dial(mongoURL)
	if err != nil {
		panic(err)
	}
	// Set the DB to the app struct for later use
	db.DB = session.DB(databaseInfo.DBName)
	db.Session = session
}

func (db *database) DBConnection() *mgo.Database {
	return db.DB
}
