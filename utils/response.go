package utils

import (
	"encoding/json"
	"net/http"
)

// Response object for later use
var Response = new(response)

type response struct{}

// MessageResponse structure for http message response
type MessageResponse struct {
	Message    interface{} `json:"msg"`
	StatusCode int         `json:"statusCode"`
}

type MessageErrorResponse struct {
	Message    interface{}       `json:"msg"`
	Errors     map[string]string `json:"errors"`
	StatusCode int               `json:"statusCode"`
}

// SendJSON sends json data as response
func (r response) SendJSON(w http.ResponseWriter, data interface{}, statusCode int) {
	// Add json content type to response header
	w.Header().Set("Content-Type", "application/json")
	// Add status code to response header
	w.WriteHeader(statusCode)
	// Encode the data into the http.ResponseWriter
	json.NewEncoder(w).Encode(&data)
}

// SuccessMessage sends json data with message and http OK status
func (r response) SuccessMessage(w http.ResponseWriter, message interface{}) {
	Response.SendMessage(w, message, http.StatusOK)
}

// SendMessage sends json data with message and custom http status code
func (r response) SendMessage(w http.ResponseWriter, message interface{}, statusCode int) {
	// Create a new instance of the message response
	responseMessage := MessageResponse{
		Message:    message,
		StatusCode: statusCode,
	}
	// Send message response
	Response.SendJSON(w, responseMessage, statusCode)
}

// SendMessage sends json data with message and custom http status code
func (r response) SendErrors(w http.ResponseWriter, errors map[string]string, statusCode int) {
	// Create a new instance of the message response
	responseMessage := MessageErrorResponse{
		Message:    "Invalid request",
		Errors:     errors,
		StatusCode: statusCode,
	}
	// Send message response
	Response.SendJSON(w, responseMessage, statusCode)
}
