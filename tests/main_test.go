package tests

import (
	"encoding/json"
	"io"
	"os"
	"resume-api/models"
	"resume-api/utils"
	"testing"
	"time"

	"gopkg.in/mgo.v2/bson"

	jwt "github.com/dgrijalva/jwt-go"

	"resume-api/app"

	"net/http"
	"net/http/httptest"
)

var testApp = app.App{}

func TestMain(m *testing.M) {
	// Create an object for the database information
	// databaseInfo := &utils.DatabaseInfo{
	// 	Username: "admin",
	// 	Password: "test",
	// 	Host:     "ds123381.mlab.com",
	// 	Port:     23381,
	// 	DBName:   "resume-testing",
	// }
	databaseInfo := &utils.DatabaseInfo{
		Username: "test_user",
		Password: "test_pass",
		Host:     "localhost",
		Port:     27017,
		DBName:   "test_db",
	}
	// Initialize the databse
	utils.Database.InitializeDatabase(databaseInfo)
	// Intiailize s3 bucket
	defer utils.Database.Session.Close()
	code := m.Run()
	// Remove all records made during testing
	clearDatabase()
	// Close test code
	os.Exit(code)
}

func clearDatabase() {
	utils.Database.DB.DropDatabase()
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	testApp.InitializeRouting()
	testApp.Router.ServeHTTP(rr, req)
	return rr
}

func checkStatusCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d but got %d.\n", expected, actual)
	}
}

func decodeResponseMessageJSON(data io.Reader) utils.MessageResponse {
	var responseData utils.MessageResponse
	json.NewDecoder(data).Decode(&responseData)
	return responseData
}

func decodeResponseErrorsJSON(data io.Reader) utils.MessageErrorResponse {
	var responseData utils.MessageErrorResponse
	json.NewDecoder(data).Decode(&responseData)
	return responseData
}

func createWorkingTestUser() (bson.ObjectId, error) {
	// Create new user
	testUser := models.NewUser{
		FullName:             "Test User",
		JobTitle:             "Tester",
		CountryCode:          "GB",
		Username:             "testuser",
		Email:                "testuser@test.com",
		Password:             "testpassword",
		PasswordConfirmation: "testpassword",
	}
	newUserID, err := models.UserModel.Create(&testUser)
	return newUserID, err
}

func generateJWTTokenForTestUser() (string, bson.ObjectId) {
	// Create a test user
	newUserID, err := createWorkingTestUser()
	if err != nil {
		panic(err)
	}
	// Get ID of this newly created user
	// Generate a new token based on this test user data
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["user_id"] = newUserID
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	signedToken, err := token.SignedString(utils.Config.JWTSecret())
	if err != nil {
		panic(err)
	}
	return signedToken, newUserID
}
