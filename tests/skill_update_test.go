package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	"bytes"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUpdateSkillItem(t *testing.T) {
	Convey("Update award item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create item for user
		newItem := models.NewSkillData{
			Title: "PHP",
			Level: "Expert",
		}
		item, err := models.SkillModel.CreateSkillItem(testUserID.Hex(), &newItem)
		if err != nil {
			panic(err)
		}
		payload := []byte(`{
			"title": "Java",
			"level": "Moderate"
		}`)
		req, _ := http.NewRequest("PUT", fmt.Sprintf("/skill/%s", item.ID.Hex()), bytes.NewBuffer(payload))
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Skill item updated")
			// Check to make sure item updated in database
			foundItem, err := models.SkillModel.FindSkillItemByData(bson.M{"_id": bson.ObjectIdHex(item.ID.Hex())})
			So(err, ShouldEqual, nil)
			So(foundItem.Title, ShouldEqual, "Java")
			So(foundItem.Level, ShouldEqual, "Moderate")
		})

	})
}
