package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestWorkHistoryCreateAndUpdateDataValidations(t *testing.T) {
	Convey("Test data validation for create and update work history routes", t, func() {
		type routeTest struct {
			routeName string
			method    string
		}

		workHistoryRoutes := []routeTest{
			{routeName: "/work-history", method: "POST"},
			{routeName: "/work-history/%s", method: "PUT"},
		}

		for _, route := range workHistoryRoutes {
			Convey(fmt.Sprintf("%s - Data validation tests", route.routeName), func() {

				// Create test user
				// Create a working jwt token
				token, testUserID := generateJWTTokenForTestUser()

				// Create new work history
				newWorkHistoryData := models.NewWorkHistoryData{
					JobTitle:   "Web Developer",
					Employer:   "Google",
					Location:   "Remote",
					StartMonth: "Janurary",
					StartYear:  2017,
					Ended:      false,
					Summary:    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper.",
				}
				newWorkHistory, err := models.WorkHistoryModel.CreateWorkHistory(testUserID.Hex(), &newWorkHistoryData)
				if err != nil {
					panic(err)
				}

				var actualRoute string
				if route.method == "POST" {
					actualRoute = route.routeName
				}
				if route.method == "PUT" {
					actualRoute = fmt.Sprintf(route.routeName, newWorkHistory.ID.Hex())
				}

				Convey("Empty data", func() {
					req, _ := http.NewRequest(route.method, actualRoute, nil)
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseMessageJSON(response.Body)
					Convey("Bad request status code with message", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Please send some data")
					})
				})

				Convey("Empty JSON data", func() {
					payload := []byte(`{}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["jobTitle"], ShouldEqual, "Job title is required")
						So(responseData.Errors["employer"], ShouldEqual, "Employer name is required")
						So(responseData.Errors["location"], ShouldEqual, "Job location is required")
						So(responseData.Errors["startMonth"], ShouldEqual, "Start month is required")
						So(responseData.Errors["startYear"], ShouldEqual, "Start year is required")
						So(responseData.Errors["summary"], ShouldEqual, "Job summary is required")
					})
				})

				Convey("Long job title error", func() {
					payload := []byte(`{
						"jobTitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare eget odio metus."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["jobTitle"], ShouldEqual, "Job title can be maximum of 80 characters long")
					})
				})

				Convey("Long employer name error", func() {
					payload := []byte(`{
						"employer": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare eget odio metus."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["employer"], ShouldEqual, "Employer name can be maximum of 80 characters long")
					})
				})

				Convey("Long location error", func() {
					payload := []byte(`{
						"location": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare eget odio metus Phasellus ornare eget odio metus."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["location"], ShouldEqual, "Job location can be maximum of 100 characters long")
					})
				})

				Convey("Long start month error", func() {
					payload := []byte(`{
						"startMonth": "Lorem ipsum dolor sit amet consectetur adipiscing elit hasellus ornare eget odio metus Phasellus ornare eget odio metus"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["startMonth"], ShouldEqual, "Start month can be maximum of 10 characters long")
					})
				})

				Convey("Invalid start month error", func() {
					payload := []byte(`{
						"startMonth": "sd032£@$*"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["startMonth"], ShouldEqual, "Start month can only contain letters")
					})
				})

				Convey("Invalid start year error", func() {
					payload := []byte(`{
						"startYear": "lorem"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid data")
					})
				})

				Convey("End month and year required error", func() {
					payload := []byte(`{
						"jobTitle": "Wordpress Expert",
						"employer": "Codeable",
						"location": "Remote",
						"startMonth": "December",
						"startYear": 2016,
						"summary": "lorem",
						"ended": true
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["endMonth"], ShouldEqual, "End month is required")
						So(responseData.Errors["endYear"], ShouldEqual, "End year is required")
					})
				})

				Convey("Long end month error", func() {
					payload := []byte(`{
						"jobTitle": "Wordpress Expert",
						"employer": "Codeable",
						"location": "Remote",
						"startMonth": "December",
						"startYear": 2016,
						"summary": "lorem",
						"ended": true,
						"endMonth": "lorem ipsum dolor sit amet consectetur adipiscing elit hasellus ornare eget odio metus phasellus ornare eget odi"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["endMonth"], ShouldEqual, "End month can be maximum of 10 characters long")
					})
				})

				Convey("Invalid end month error", func() {
					payload := []byte(`{
						"jobTitle": "Wordpress Expert",
						"employer": "Codeable",
						"location": "Remote",
						"startMonth": "December",
						"startYear": 2016,
						"summary": "lorem",
						"ended": true,
						"endMonth": "234@£42"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["endMonth"], ShouldEqual, "End month can only contain letters")
					})
				})

				Convey("Invalid end year error", func() {
					payload := []byte(`{
						"jobTitle": "Wordpress Expert",
						"employer": "Codeable",
						"location": "Remote",
						"startMonth": "December",
						"startYear": 2016,
						"summary": "lorem",
						"ended": true,
						"endMonth": "December",
						"endYear": "lorem"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid data")
					})
				})

				Convey("Long summary error", func() {
					payload := []byte(`{
						"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet elit gravida, blandit dui vel, commodo sapien. Quisque non vulputate augue, sed tincidunt turpis. Phasellus suscipit tortor leo, a rhoncus tortor ullamcorper eget. Praesent pharetra quam a pharetra porttitor. Ut sit amet libero scelerisque ex egestas maximus quis vel sem. Maecenas tincidunt, nunc vitae aliquam laoreet, nunc nunc lobortis eros, ut fringilla orci massa at est. In consectetur, justo a mollis auctor, turpis elit euismod felis, eget aliquam nisi nibh eget turpis. In eu placerat dolor. Vivamus a tincidunt sed."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["summary"], ShouldEqual, "Job summary can be maximum of 500 characters long")
					})
				})

			})
		}
	})
}
