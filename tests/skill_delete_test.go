package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDeleteSkillItem(t *testing.T) {
	Convey("Delete skill item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create award item for user
		newItem := models.NewSkillData{
			Title: "PHP",
			Level: "Expert",
		}
		item, err := models.SkillModel.CreateSkillItem(testUserID.Hex(), &newItem)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/skill/%s", item.ID.Hex()), nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Skill item deleted")
			// Check to make sure item doesnt exist in database
			_, err := models.SkillModel.FindSkillItemByData(bson.M{"_id": bson.ObjectIdHex(item.ID.Hex())})
			So(err, ShouldNotEqual, nil)
		})

	})
}
