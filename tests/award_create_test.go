package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateAwardItem(t *testing.T) {
	// Create a working jwt token
	token, testUserID := generateJWTTokenForTestUser()

	Convey("Test add award item", t, func() {

		Convey("Add valid award item", func() {
			payload := []byte(`{
				"title": "Best Web Developer",
				"institution": "Web Awards",
				"website": "http://google.com",
				"month": "September",
				"year": 2015,
				"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat ac, molestie pulvinar felis. Integer hendrerit et erat vel tristique. Morbi id elit eu ligula varius volutpat. Etiam mollis felis sit amet nunc consequat metus."
			}`)
			req, _ := http.NewRequest("POST", "/award", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Created request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusCreated)
				So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Award item created")
			})
			foundAwardItem, err := models.AwardModel.FindAwardItemByData(bson.M{
				"userID": bson.ObjectIdHex(testUserID.Hex()),
			})
			So(err, ShouldEqual, nil)
			So(foundAwardItem.Title, ShouldEqual, "Best Web Developer")
			So(foundAwardItem.Institution, ShouldEqual, "Web Awards")
			So(foundAwardItem.Website, ShouldEqual, "http://google.com")
			So(foundAwardItem.Month, ShouldEqual, "September")
			So(foundAwardItem.Year, ShouldEqual, 2015)
			So(foundAwardItem.Summary, ShouldEqual, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat ac, molestie pulvinar felis. Integer hendrerit et erat vel tristique. Morbi id elit eu ligula varius volutpat. Etiam mollis felis sit amet nunc consequat metus.")
		})

	})

}
