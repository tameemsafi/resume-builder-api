package tests

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"crypto/md5"

	"resume-api/models"
	"resume-api/utils"

	"github.com/aws/aws-sdk-go/service/s3"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUserAvatarUpload(t *testing.T) {
	Convey("Upload user profile image", t, func() {
		// Get test profile image
		file, err := os.Open("./files/profile-img.jpg")
		if err != nil {
			t.Log(err)
			t.Fail()
		}
		defer file.Close()
		// Create request body from file
		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)
		part, err := writer.CreateFormFile("image", filepath.Base("./files/profile-img.jpg"))
		if err != nil {
			t.Log(err)
			t.Fail()
		}
		_, err = io.Copy(part, file)
		err = writer.Close()
		if err != nil {
			t.Log(err)
			t.Fail()
		}
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Public link for the profile image
		// Used to check if response is correct
		md5Hasher := md5.New()
		md5Hasher.Write([]byte(testUserID))
		bucketName := "resume-api-bucket"
		hashedFileName := hex.EncodeToString(md5Hasher.Sum(nil)) + ".jpg"
		hashedFilePath := "avatars/" + hashedFileName
		publicAvatarLink := fmt.Sprintf("%v/%v/%v", utils.AWSS3.BucketInstance().Endpoint, bucketName, hashedFilePath)
		// Send request for a new token
		req, _ := http.NewRequest("POST", "/avatar", body)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		req.Header.Set("Content-Type", writer.FormDataContentType())
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		Convey("OK status code with valid aws s3 link response", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, publicAvatarLink)
		})
		// Remove uploaded avatar from s3 bucket
		deleteParams := &s3.DeleteObjectInput{
			Bucket: &bucketName,
			Key:    &hashedFilePath,
		}
		utils.AWSS3.BucketInstance().DeleteObject(deleteParams)
		// Check profile image was updated in database
		foundUser, err := models.UserModel.FindUserByID(testUserID.Hex())
		if err != nil {
			t.Log(err.Error())
			t.Fail()
		}
		So(foundUser.ProfileImage, ShouldEqual, hashedFileName)
	})
}
