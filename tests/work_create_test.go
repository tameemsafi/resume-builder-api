package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateWorkHistory(t *testing.T) {
	// Create a working jwt token
	token, testUserID := generateJWTTokenForTestUser()

	Convey("Test add new work history", t, func() {

		Convey("Add valid work history", func() {
			payload := []byte(`{
				"jobTitle": "Wordpress Expert",
				"employer": "Codeable",
				"location": "Remote",
				"startMonth": "December",
				"startYear": 2016,
				"ended": false,
				"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat ac, molestie pulvinar felis. Integer hendrerit et erat vel tristique. Morbi id elit eu ligula varius volutpat. Etiam mollis felis sit amet nunc consequat metus."
			}`)
			req, _ := http.NewRequest("POST", "/work-history", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Created request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusCreated)
				So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Work history created")
			})
			foundWorkHistory, err := models.WorkHistoryModel.FindWorkHistoryByData(bson.M{
				"userID": bson.ObjectIdHex(testUserID.Hex()),
			})
			So(foundWorkHistory.JobTitle, ShouldEqual, "Wordpress Expert")
			So(foundWorkHistory.Employer, ShouldEqual, "Codeable")
			So(foundWorkHistory.Location, ShouldEqual, "Remote")
			So(foundWorkHistory.StartMonth, ShouldEqual, "December")
			So(foundWorkHistory.StartYear, ShouldEqual, 2016)
			So(foundWorkHistory.Ended, ShouldEqual, false)
			So(foundWorkHistory.Summary, ShouldEqual, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat ac, molestie pulvinar felis. Integer hendrerit et erat vel tristique. Morbi id elit eu ligula varius volutpat. Etiam mollis felis sit amet nunc consequat metus.")
			So(err, ShouldEqual, nil)
		})

	})

}
