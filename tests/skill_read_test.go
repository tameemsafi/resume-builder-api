package tests

import (
	"fmt"
	"net/http"
	"resume-api/controllers"
	"resume-api/models"
	"testing"

	"encoding/json"

	"reflect"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReadSkillItems(t *testing.T) {
	Convey("Read all skill items", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create work histories for user
		newOne := models.NewSkillData{
			Title: "PHP",
			Level: "Expert",
		}
		newTwo := models.NewSkillData{
			Title: "Java",
			Level: "Expert",
		}
		newThree := models.NewSkillData{
			Title: "Golang",
			Level: "Expert",
		}
		itemOne, err := models.SkillModel.CreateSkillItem(testUserID.Hex(), &newOne)
		if err != nil {
			panic(err)
		}
		itemTwo, err := models.SkillModel.CreateSkillItem(testUserID.Hex(), &newTwo)
		if err != nil {
			panic(err)
		}
		itemThree, err := models.SkillModel.CreateSkillItem(testUserID.Hex(), &newThree)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("GET", "/skills", nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		var responseData controllers.SkillItemsResponse
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			for k, responseItem := range responseData.SkillItems {
				var validReflection reflect.Value
				if k == 0 {
					validReflection = reflect.ValueOf(&itemOne).Elem()
				}
				if k == 1 {
					validReflection = reflect.ValueOf(&itemTwo).Elem()
				}
				if k == 2 {
					validReflection = reflect.ValueOf(&itemThree).Elem()
				}
				reflection := reflect.ValueOf(&responseItem).Elem()
				for i := 0; i < reflection.NumField(); i++ {
					So(reflection.Field(i).String(), ShouldEqual, validReflection.Field(i).String())
				}
			}
		})
	})
}
