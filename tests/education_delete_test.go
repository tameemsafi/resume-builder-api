package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDeleteEducationItem(t *testing.T) {
	Convey("Delete education item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create education item for user
		newEducationItem := models.NewEducationData{
			Institution: "University of Essex",
			Location:    "Colchester, Essex",
			StartMonth:  "January",
			StartYear:   2015,
			Ended:       false,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		educationItem, err := models.EducationModel.CreateEducationItem(testUserID.Hex(), &newEducationItem)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/education/%s", educationItem.ID.Hex()), nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Education item deleted")
			// Check to make sure education item doesnt exist in database
			_, err := models.EducationModel.FindEducationItemByData(bson.M{"_id": bson.ObjectIdHex(educationItem.ID.Hex())})
			So(err, ShouldNotEqual, nil)
		})

	})
}
