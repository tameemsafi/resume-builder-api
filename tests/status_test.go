package tests

import (
	"encoding/json"
	"net/http"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestStatusHandler(t *testing.T) {
	req, _ := http.NewRequest("GET", "/status", nil)
	response := executeRequest(req)

	Convey("Test API status route", t, func() {

		Convey("OK status", func() {
			So(http.StatusOK, ShouldEqual, response.Code)
		})

		Convey("Running response message", func() {
			var responseData map[string]interface{}
			json.NewDecoder(response.Body).Decode(&responseData)
			So(responseData["msg"], ShouldEqual, "API server is runing")
			So(responseData["statusCode"], ShouldEqual, http.StatusOK)
		})

	})

}
