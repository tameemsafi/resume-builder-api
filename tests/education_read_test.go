package tests

import (
	"fmt"
	"net/http"
	"resume-api/controllers"
	"resume-api/models"
	"testing"

	"encoding/json"

	"reflect"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReadEducationItems(t *testing.T) {
	Convey("Read all education items", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create education items for user
		newEducationItemOne := models.NewEducationData{
			Institution: "University of Essex",
			Location:    "Colchester, Essex",
			StartMonth:  "January",
			StartYear:   2012,
			Ended:       false,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		newEducationItemTwo := models.NewEducationData{
			Institution: "University of Oxford",
			Location:    "City of Oxford",
			StartMonth:  "January",
			StartYear:   2014,
			Ended:       false,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		newEducationItemThree := models.NewEducationData{
			Institution: "University of Cambridge",
			Location:    "City of Cambridge",
			StartMonth:  "January",
			StartYear:   2016,
			Ended:       false,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		educationItemOne, err := models.EducationModel.CreateEducationItem(testUserID.Hex(), &newEducationItemOne)
		if err != nil {
			panic(err)
		}
		educationItemTwo, err := models.EducationModel.CreateEducationItem(testUserID.Hex(), &newEducationItemTwo)
		if err != nil {
			panic(err)
		}
		educationItemThree, err := models.EducationModel.CreateEducationItem(testUserID.Hex(), &newEducationItemThree)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("GET", "/educations", nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		var responseData controllers.EducationItemsResponse
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			for k, responseEducationItem := range responseData.EducationItems {
				var validReflection reflect.Value
				if k == 0 {
					validReflection = reflect.ValueOf(&educationItemOne).Elem()
				}
				if k == 1 {
					validReflection = reflect.ValueOf(&educationItemTwo).Elem()
				}
				if k == 2 {
					validReflection = reflect.ValueOf(&educationItemThree).Elem()
				}
				reflection := reflect.ValueOf(&responseEducationItem).Elem()
				for i := 0; i < reflection.NumField(); i++ {
					So(reflection.Field(i).String(), ShouldEqual, validReflection.Field(i).String())
				}
			}
		})
	})
}
