package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	"bytes"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUpdateWorkHistory(t *testing.T) {
	Convey("Update work history item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create item for user
		newWorkHistory := models.NewWorkHistoryData{
			JobTitle:   "Technician",
			Employer:   "Apple",
			Location:   "San Francisco",
			StartMonth: "January",
			StartYear:  2014,
			EndMonth:   "December",
			EndYear:    2014,
			Ended:      true,
			Summary:    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit li.",
		}
		workHistory, err := models.WorkHistoryModel.CreateWorkHistory(testUserID.Hex(), &newWorkHistory)
		if err != nil {
			panic(err)
		}
		payload := []byte(`{
			"jobTitle": "Google Developer",
			"employer": "Google",
			"location": "Remote",
			"startMonth": "February",
			"startYear": 2016,
			"ended": true,
			"endMonth": "January",
			"endYear": 2017,
			"summary": "Everything is good"
		}`)
		req, _ := http.NewRequest("PUT", fmt.Sprintf("/work-history/%s", workHistory.ID.Hex()), bytes.NewBuffer(payload))
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Work history updated")
			// Check to make sure item updated in database
			foundWorkHistory, err := models.WorkHistoryModel.FindWorkHistoryByData(bson.M{"_id": bson.ObjectIdHex(workHistory.ID.Hex())})
			So(err, ShouldEqual, nil)
			So(foundWorkHistory.JobTitle, ShouldEqual, "Google Developer")
			So(foundWorkHistory.Employer, ShouldEqual, "Google")
			So(foundWorkHistory.Location, ShouldEqual, "Remote")
			So(foundWorkHistory.StartMonth, ShouldEqual, "February")
			So(foundWorkHistory.StartYear, ShouldEqual, 2016)
			So(foundWorkHistory.EndMonth, ShouldEqual, "January")
			So(foundWorkHistory.EndYear, ShouldEqual, 2017)
			So(foundWorkHistory.Ended, ShouldEqual, true)
			So(foundWorkHistory.Summary, ShouldEqual, "Everything is good")
		})

	})
}
