package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDeleteAwardItem(t *testing.T) {
	Convey("Delete award item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create award item for user
		newAWardItem := models.NewAwardData{
			Title:       "Best Award",
			Institution: "University of Essex",
			Website:     "http://google.com",
			Month:       "January",
			Year:        2015,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		awardItem, err := models.AwardModel.CreateAwardItem(testUserID.Hex(), &newAWardItem)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/award/%s", awardItem.ID.Hex()), nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Award item deleted")
			// Check to make sure award item doesnt exist in database
			_, err := models.AwardModel.FindAwardItemByData(bson.M{"_id": bson.ObjectIdHex(awardItem.ID.Hex())})
			So(err, ShouldNotEqual, nil)
		})

	})
}
