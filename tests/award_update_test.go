package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	"bytes"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUpdateAwardItem(t *testing.T) {
	Convey("Update award item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create award item for user
		newAwardItem := models.NewAwardData{
			Title:       "Best Award",
			Institution: "University of Essex",
			Website:     "http://google.com",
			Month:       "January",
			Year:        2015,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		awardItem, err := models.AwardModel.CreateAwardItem(testUserID.Hex(), &newAwardItem)
		if err != nil {
			panic(err)
		}
		payload := []byte(`{
			"title": "Google Award",
			"institution": "Apple",
			"website": "http://facebook.com",
			"month": "February",
			"year": 2016,
			"summary": "Everything is good"
		}`)
		req, _ := http.NewRequest("PUT", fmt.Sprintf("/award/%s", awardItem.ID.Hex()), bytes.NewBuffer(payload))
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Award item updated")
			// Check to make sure item updated in database
			foundAwardItem, err := models.AwardModel.FindAwardItemByData(bson.M{"_id": bson.ObjectIdHex(awardItem.ID.Hex())})
			So(err, ShouldEqual, nil)
			So(foundAwardItem.Title, ShouldEqual, "Google Award")
			So(foundAwardItem.Institution, ShouldEqual, "Apple")
			So(foundAwardItem.Website, ShouldEqual, "http://facebook.com")
			So(foundAwardItem.Month, ShouldEqual, "February")
			So(foundAwardItem.Year, ShouldEqual, 2016)
			So(foundAwardItem.Summary, ShouldEqual, "Everything is good")
		})

	})
}
