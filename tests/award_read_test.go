package tests

import (
	"fmt"
	"net/http"
	"resume-api/controllers"
	"resume-api/models"
	"testing"

	"encoding/json"

	"reflect"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReadAwardItems(t *testing.T) {
	Convey("Read all award items", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create work histories for user
		newAwardItemOne := models.NewAwardData{
			Title:       "Best Award 1",
			Institution: "University of Essex",
			Website:     "http://amazon.com",
			Month:       "January",
			Year:        2012,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		newAwardItemTwo := models.NewAwardData{
			Title:       "Best Award 1",
			Institution: "University of Essex",
			Website:     "http://apple.com",
			Month:       "January",
			Year:        2012,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		newAwardItemThree := models.NewAwardData{
			Title:       "Best Award 1",
			Institution: "University of Essex",
			Website:     "http://google.com",
			Month:       "January",
			Year:        2012,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		awardItemOne, err := models.AwardModel.CreateAwardItem(testUserID.Hex(), &newAwardItemOne)
		if err != nil {
			panic(err)
		}
		awardItemTwo, err := models.AwardModel.CreateAwardItem(testUserID.Hex(), &newAwardItemTwo)
		if err != nil {
			panic(err)
		}
		awardItemThree, err := models.AwardModel.CreateAwardItem(testUserID.Hex(), &newAwardItemThree)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("GET", "/awards", nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		var responseData controllers.AwardItemsResponse
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			for k, responseAwardItem := range responseData.AwardItems {
				var validReflection reflect.Value
				if k == 0 {
					validReflection = reflect.ValueOf(&awardItemOne).Elem()
				}
				if k == 1 {
					validReflection = reflect.ValueOf(&awardItemTwo).Elem()
				}
				if k == 2 {
					validReflection = reflect.ValueOf(&awardItemThree).Elem()
				}
				reflection := reflect.ValueOf(&responseAwardItem).Elem()
				for i := 0; i < reflection.NumField(); i++ {
					So(reflection.Field(i).String(), ShouldEqual, validReflection.Field(i).String())
				}
			}
		})
	})
}
