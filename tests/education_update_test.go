package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	"bytes"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUpdateEducationItem(t *testing.T) {
	Convey("Update education item", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create item for user
		newEducationItem := models.NewEducationData{
			Institution: "University of Essex",
			Location:    "Colchester, Essex",
			StartMonth:  "September",
			StartYear:   2015,
			Ended:       false,
			Summary:     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat",
		}
		educationItem, err := models.EducationModel.CreateEducationItem(testUserID.Hex(), &newEducationItem)
		if err != nil {
			panic(err)
		}
		payload := []byte(`{
			"institution": "Google Award",
			"location": "Apple",
			"startMonth": "February",
			"startYear": 2016,
			"ended": true,
			"endMonth": "January",
			"endYear": 2017,
			"summary": "Everything is good"
		}`)
		req, _ := http.NewRequest("PUT", fmt.Sprintf("/education/%s", educationItem.ID.Hex()), bytes.NewBuffer(payload))
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Education item updated")
			// Check to make sure item updated in database
			foundEducationItem, err := models.EducationModel.FindEducationItemByData(bson.M{"_id": bson.ObjectIdHex(educationItem.ID.Hex())})
			So(err, ShouldEqual, nil)
			So(foundEducationItem.Institution, ShouldEqual, "Google Award")
			So(foundEducationItem.Location, ShouldEqual, "Apple")
			So(foundEducationItem.StartMonth, ShouldEqual, "February")
			So(foundEducationItem.StartYear, ShouldEqual, 2016)
			So(foundEducationItem.EndMonth, ShouldEqual, "January")
			So(foundEducationItem.EndYear, ShouldEqual, 2017)
			So(foundEducationItem.Ended, ShouldEqual, true)
			So(foundEducationItem.Summary, ShouldEqual, "Everything is good")
		})

	})
}
