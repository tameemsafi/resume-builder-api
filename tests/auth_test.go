package tests

import (
	"resume-api/utils"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"bytes"
	"net/http"

	. "github.com/smartystreets/goconvey/convey"
)

func TestAuthControllerCreate(t *testing.T) {

	Convey("Test new user registration", t, func() {
		payload := []byte(`{
			"fullname": "Tameem Safi",
			"jobTitle": "Full-stack web developer",
			"countryCode": "GB",
			"username": "tameemsafi",
			"email": "tameem@safi.me.uk",
			"password": "google",
			"passwordConfirmation": "google"
		}`)
		req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		Convey("Created request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusCreated)
			So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Account created")
		})
		Convey("Check acccount exists in database", func() {
			accountCount, err := utils.Database.DB.C("users").Find(bson.M{
				"fullName":    "Tameem Safi",
				"jobTitle":    "Full-stack web developer",
				"countryCode": "GB",
				"username":    "tameemsafi",
				"email":       "tameem@safi.me.uk",
			}).Count()
			if err != nil {
				t.Log("Database error")
				t.Fail()
			}
			So(accountCount, ShouldEqual, 1)
		})
	})

	Convey("Test new user invalid requests", t, func() {

		Convey("No data sent gives error", func() {
			req, _ := http.NewRequest("POST", "/register", nil)
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Please send some data")
			})
		})

		Convey("All blank fields gives validation errors", func() {
			payload := []byte(`{
				"fullname": null,
				"jobTitle": null,
				"countryCode": null,
				"username": null,
				"email": null,
				"password": null,
				"passwordConfirmation": null
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["fullName"], ShouldEqual, "Fullname is required")
				So(responseData.Errors["jobTitle"], ShouldEqual, "Job title is required")
				So(responseData.Errors["countryCode"], ShouldEqual, "Country code is required")
				So(responseData.Errors["username"], ShouldEqual, "Username is required")
				So(responseData.Errors["email"], ShouldEqual, "Email address is required")
				So(responseData.Errors["password"], ShouldEqual, "Password is required")
				So(responseData.Errors["passwordConfirmation"], ShouldEqual, "Password confirmation is required")
			})
		})

		Convey("Fullname too long error", func() {
			payload := []byte(`{
				"fullname": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consequat faucibus ex, ut cras amet."
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["fullName"], ShouldEqual, "Fullname can be maximum of 60 characters long")
			})
		})

		Convey("Job title too long error", func() {
			payload := []byte(`{
				"jobTitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consequat faucibus ex, ut cras amet aucibus ex, ut cras amet."
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["jobTitle"], ShouldEqual, "Job title can be maximum of 100 characters long")
			})
		})

		Convey("Country code alpha-2 error", func() {
			payload := []byte(`{
				"countryCode": "24@£"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["countryCode"], ShouldEqual, "Country code can only be iso alpha-2 characters")
			})
		})

		Convey("Username alphanumeric error", func() {
			payload := []byte(`{
				"username": "pearnuts@$££"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["username"], ShouldEqual, "Username can only contain letters and numbers")
			})
		})

		Convey("Username too long error", func() {
			payload := []byte(`{
				"username": "Loremipsumdolorsitametconsect"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["username"], ShouldEqual, "Username can be maximum of 20 characters long")
			})
		})

		Convey("Email invalid error", func() {
			payload := []byte(`{
				"email": "gary"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["email"], ShouldEqual, "Email address is invalid")
			})
		})

		Convey("Email invalid error 2", func() {
			payload := []byte(`{
				"email": "gary@ggoo"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["email"], ShouldEqual, "Email address is invalid")
			})
		})

		Convey("Password invalid error", func() {
			payload := []byte(`{
				"password": "asdfsadfasdf@! sd4d"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["password"], ShouldEqual, "Password is invalid")
			})
		})

		Convey("Password min length error", func() {
			payload := []byte(`{
				"password": "abc"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["password"], ShouldEqual, "Password must be between 5 and 20 characters long")
			})
		})

		Convey("Password max length error", func() {
			payload := []byte(`{
				"password": "abcabcabcabcabcabcabcabcabc"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["password"], ShouldEqual, "Password must be between 5 and 20 characters long")
			})
		})

		Convey("Password confirmation invalid error", func() {
			payload := []byte(`{
				"passwordConfirmation": "asdfsadfasdf@! sd4d"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["passwordConfirmation"], ShouldEqual, "Password confirmation is invalid")
			})
		})

		Convey("Password confirmation min length error", func() {
			payload := []byte(`{
				"passwordConfirmation": "abc"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["passwordConfirmation"], ShouldEqual, "Password confirmation must be between 5 and 20 characters long")
			})
		})

		Convey("Password confirmation max length error", func() {
			payload := []byte(`{
				"passwordConfirmation": "abcabcabcabcabcabcabcabcabc"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["passwordConfirmation"], ShouldEqual, "Password confirmation must be between 5 and 20 characters long")
			})
		})

		Convey("Password match confirmation error", func() {
			payload := []byte(`{
				"fullname": "Tameem Safi",
				"jobTitle": "Full-stack web developer",
				"countryCode": "GB",
				"username": "tameemsafi2",
				"email": "tameem2@safi.me.uk",
				"password": "google",
				"passwordConfirmation": "google2"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Errors["password"], ShouldEqual, "Password and confirmation must match")
				So(responseData.Errors["passwordConfirmation"], ShouldEqual, "Password and confirmation must match")
			})
		})

		Convey("Duplicate username check", func() {
			// Create first user
			payload := []byte(`{
				"fullname": "Tameem Safi",
				"jobTitle": "Full-stack web developer",
				"countryCode": "GB",
				"username": "tameemsafi3",
				"email": "tameem4@safi.me.uk",
				"password": "google",
				"passwordConfirmation": "google"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Username test - account created", func() {
				So(response.Code, ShouldEqual, http.StatusCreated)
				So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Account created")
			})
			Convey("Check acccount exists in database", func() {
				accountCount, err := utils.Database.DB.C("users").Find(bson.M{
					"fullName":    "Tameem Safi",
					"jobTitle":    "Full-stack web developer",
					"countryCode": "GB",
					"username":    "tameemsafi3",
					"email":       "tameem4@safi.me.uk",
				}).Count()
				if err != nil {
					t.Log("Database error")
					t.Fail()
				}
				So(accountCount, ShouldEqual, 1)
			})
			// Create second account with the same username
			payload = []byte(`{
				"fullname": "Tameem Safi",
				"jobTitle": "Full-stack web developer",
				"countryCode": "GB",
				"username": "tameemsafi3",
				"email": "tameem5@safi.me.uk",
				"password": "google",
				"passwordConfirmation": "google"
			}`)
			req, _ = http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response = executeRequest(req)
			responseData = decodeResponseMessageJSON(response.Body)
			Convey("Username exists message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Username already exists")
			})
		})

		Convey("Duplicate email check", func() {
			// Create first user
			payload := []byte(`{
				"fullname": "Tameem Safi",
				"jobTitle": "Full-stack web developer",
				"countryCode": "GB",
				"username": "tameemsafi4",
				"email": "tameem6@safi.me.uk",
				"password": "google",
				"passwordConfirmation": "google"
			}`)
			req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Email test - Account created message", func() {
				So(response.Code, ShouldEqual, http.StatusCreated)
				So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Account created")
			})
			Convey("Check acccount exists in database", func() {
				accountCount, err := utils.Database.DB.C("users").Find(bson.M{
					"fullName":    "Tameem Safi",
					"jobTitle":    "Full-stack web developer",
					"countryCode": "GB",
					"username":    "tameemsafi4",
					"email":       "tameem6@safi.me.uk",
				}).Count()
				if err != nil {
					t.Log("Database error")
					t.Fail()
				}
				So(accountCount, ShouldEqual, 1)
			})
			// Create second account with the same username
			payload = []byte(`{
				"fullname": "Tameem Safi",
				"jobTitle": "Full-stack web developer",
				"countryCode": "GB",
				"username": "tameemsafi5",
				"email": "tameem6@safi.me.uk",
				"password": "google",
				"passwordConfirmation": "google"
			}`)
			req, _ = http.NewRequest("POST", "/register", bytes.NewBuffer(payload))
			response = executeRequest(req)
			responseData = decodeResponseMessageJSON(response.Body)
			Convey("Email exists message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Email address already exists")
			})
		})

	})

}

func TestAuthControllerLogin(t *testing.T) {

	Convey("Test user login system", t, func() {

		Convey("Invalid data response", func() {
			req, _ := http.NewRequest("POST", "/login", nil)
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Please send some data")
			})
		})

		Convey("Empty data response", func() {
			payload := []byte(`{
				"username": "",
				"password": ""
			}`)
			req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["username"], ShouldEqual, "Username is required")
				So(responseData.Errors["password"], ShouldEqual, "Password is required")
			})
		})

		Convey("Fake email response", func() {
			payload := []byte(`{
				"username": "fakeuser@test.com",
				"password": "testing"
			}`)
			req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "User does not exist")
			})
		})

		Convey("Fake username response", func() {
			payload := []byte(`{
				"username": "fakeuser",
				"password": "testing"
			}`)
			req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Bad request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "User does not exist")
			})
		})

		Convey("Login tests", func() {
			// Create new user
			createWorkingTestUser()

			Convey("Wrong password", func() {
				// Send wrong password request
				payload := []byte(`{
					"username": "testuser",
					"password": "wrongpassword"
				}`)
				req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
				response := executeRequest(req)
				responseData := decodeResponseMessageJSON(response.Body)
				Convey("Bad request status code with message", func() {
					So(response.Code, ShouldEqual, http.StatusBadRequest)
					So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
					So(responseData, ShouldNotBeEmpty)
					So(responseData.Message, ShouldEqual, "Password is incorrect")
				})
			})

			Convey("Correct username, wrong password", func() {
				// Send wrong password request
				payload := []byte(`{
					"username": "testuser@test.com",
					"password": "wrongpassword"
				}`)
				req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
				response := executeRequest(req)
				responseData := decodeResponseMessageJSON(response.Body)
				Convey("Bad request status code with message", func() {
					So(response.Code, ShouldEqual, http.StatusBadRequest)
					So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
					So(responseData, ShouldNotBeEmpty)
					So(responseData.Message, ShouldEqual, "Password is incorrect")
				})
			})

			Convey("Correct email, wrong password", func() {
				// Send wrong password request
				payload := []byte(`{
					"username": "testuser@test.com",
					"password": "wrongpassword"
				}`)
				req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
				response := executeRequest(req)
				responseData := decodeResponseMessageJSON(response.Body)
				Convey("Bad request status code with message", func() {
					So(response.Code, ShouldEqual, http.StatusBadRequest)
					So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
					So(responseData, ShouldNotBeEmpty)
					So(responseData.Message, ShouldEqual, "Password is incorrect")
				})
			})

			Convey("Correct username and password", func() {
				// Send wrong password request
				payload := []byte(`{
					"username": "testuser",
					"password": "testpassword"
				}`)
				req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
				response := executeRequest(req)
				responseData := decodeResponseMessageJSON(response.Body)
				Convey("OK status code with message", func() {
					So(response.Code, ShouldEqual, http.StatusOK)
					So(responseData.StatusCode, ShouldEqual, http.StatusOK)
					So(responseData, ShouldNotBeEmpty)
				})
				Convey("JWT valid", func() {
					tokenValidationError := utils.JWT.VerifyEncodedJWT(responseData.Message.(string))
					So(tokenValidationError, ShouldEqual, nil)
				})
			})

		})

	})
}
