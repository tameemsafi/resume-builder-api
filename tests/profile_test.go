package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestProfileUpdate(t *testing.T) {
	Convey("Updating profile information", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		Convey("Update user with valid information", func() {
			payload := []byte(`{
				"fullName": "Gary Miller",
				"phoneNumber": "5033946143",
				"jobTitle": "Web developer",
				"countryCode": "US",
				"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in nunc vitae dui laoreet interdum. Duis sem turpis, aliquam vitae ipsum quis, accumsan ullamcorper mi. Suspendisse tempus lacinia felis quis suscipit. Nam sollicitudin, quam sit amet porttitor ornare."
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Updated status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusOK)
				So(responseData.StatusCode, ShouldEqual, http.StatusOK)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Profile updated")
			})

			Convey("New profile info should be in database", func() {
				foundUser, _ := models.UserModel.FindUserByID(testUserID.Hex())
				So(foundUser.FullName, ShouldEqual, "Gary Miller")
				So(foundUser.JobTitle, ShouldEqual, "Web developer")
				So(foundUser.CountryCode, ShouldEqual, "US")
				So(foundUser.Summary, ShouldEqual, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in nunc vitae dui laoreet interdum. Duis sem turpis, aliquam vitae ipsum quis, accumsan ullamcorper mi. Suspendisse tempus lacinia felis quis suscipit. Nam sollicitudin, quam sit amet porttitor ornare.")
			})
		})

		Convey("Empty data sent to profile update", func() {
			req, _ := http.NewRequest("PUT", "/profile", nil)
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Send some data message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Please send some data")
			})
		})

		Convey("No data sent with json to profile update", func() {
			payload := []byte(`{}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("All fields required message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["fullName"], ShouldEqual, "Fullname is required")
				So(responseData.Errors["phoneNumber"], ShouldEqual, "Phone number is required")
				So(responseData.Errors["jobTitle"], ShouldEqual, "Job title is required")
				So(responseData.Errors["countryCode"], ShouldEqual, "Country code is required")
				So(responseData.Errors["summary"], ShouldEqual, "Summary is required")
			})
		})

		Convey("Empty data sent with json to profile update", func() {
			payload := []byte(`{
				"fullName": "",
				"phoneNumber": "",
				"jobTitle": "",
				"countryCode": "",
				"summary": ""
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("All fields required message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["fullName"], ShouldEqual, "Fullname is required")
				So(responseData.Errors["phoneNumber"], ShouldEqual, "Phone number is required")
				So(responseData.Errors["jobTitle"], ShouldEqual, "Job title is required")
				So(responseData.Errors["countryCode"], ShouldEqual, "Country code is required")
				So(responseData.Errors["summary"], ShouldEqual, "Summary is required")
			})
		})

		Convey("Long fullname sent with json to profile update", func() {
			payload := []byte(`{
				"fullName": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in nunc vitae dui laoreet interdum. Duis sem turpis, aliquam vitae ipsum ",
				"phoneNumber": "",
				"jobTitle": "",
				"countryCode": "",
				"summary": ""
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Fullname too long message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["fullName"], ShouldEqual, "Fullname can be maximum of 60 characters long")
			})
		})

		Convey("Phone number with letters sent to profile update", func() {
			payload := []byte(`{
				"fullName": "",
				"phoneNumber": "Hello",
				"jobTitle": "",
				"countryCode": "",
				"summary": ""
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Phone number can only contain numbers message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["phoneNumber"], ShouldEqual, "Phone number can only contain numbers")
			})
		})

		Convey("Phone number too long sent to profile update", func() {
			payload := []byte(`{
				"fullName": "",
				"phoneNumber": "123123123123123123123123123123123123123123123",
				"jobTitle": "",
				"countryCode": "",
				"summary": ""
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Phone number max 20 chars message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["phoneNumber"], ShouldEqual, "Phone number can be between 1 and 20 characters long")
			})
		})

		Convey("Numbers as counry code sent to profile update", func() {
			payload := []byte(`{
				"fullName": "",
				"phoneNumber": "",
				"jobTitle": "",
				"countryCode": "12",
				"summary": ""
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Country code must be iso alpha-2 message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["countryCode"], ShouldEqual, "Country code can only be iso alpha-2 characters")
			})
		})

		Convey("Country code too long sent to profile update", func() {
			payload := []byte(`{
				"fullName": "",
				"phoneNumber": "",
				"jobTitle": "",
				"countryCode": "AAAS",
				"summary": ""
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Country code must be iso alpha-2 message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["countryCode"], ShouldEqual, "Country code can only be iso alpha-2 characters")
			})
		})

		Convey("Summary too long to profile update", func() {
			payload := []byte(`{
				"fullName": "",
				"phoneNumber": "",
				"jobTitle": "",
				"countryCode": "",
				"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum odio sed quam dignissim vehicula. Etiam pharetra mauris elit, ut consequat ante bibendum in. In hac habitasse platea dictumst. Nunc suscipit luctus risus, non euismod erat iaculis ac. In vulputate justo a lorem tincidunt posuere. Cras quis magna tortor. Sed vitae est eget velit pretium tristique. Praesent tempus imperdiet ullamcorper. Maecenas consequat ornare risus, et eleifend odio finibus sit amet. Cras fringilla, odio sed pretium imperdiet, mauris tellus consectetur orci, ac facilisis mauris dui a ipsum nullam."
			}`)
			req, _ := http.NewRequest("PUT", "/profile", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseErrorsJSON(response.Body)
			Convey("Summary too long message", func() {
				So(response.Code, ShouldEqual, http.StatusBadRequest)
				So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Invalid request")
				So(responseData.Errors["summary"], ShouldEqual, "Summary must be between 1 and 500 characters long")
			})
		})

	})
}
