package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateSkillItem(t *testing.T) {
	// Create a working jwt token
	token, testUserID := generateJWTTokenForTestUser()

	Convey("Test add skill item", t, func() {

		Convey("Add valid skill item", func() {
			payload := []byte(`{
				"title": "PHP",
				"level": "Expert"
			}`)
			req, _ := http.NewRequest("POST", "/skill", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Created request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusCreated)
				So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Skill item created")
			})
			foundSkillItem, err := models.SkillModel.FindSkillItemByData(bson.M{
				"userID": bson.ObjectIdHex(testUserID.Hex()),
			})
			So(err, ShouldEqual, nil)
			So(foundSkillItem.Title, ShouldEqual, "PHP")
			So(foundSkillItem.Level, ShouldEqual, "Expert")
		})

	})

}
