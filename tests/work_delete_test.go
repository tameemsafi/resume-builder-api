package tests

import (
	"encoding/json"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDeleteWorkHistory(t *testing.T) {
	Convey("Delete work history", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create work history for user
		newWorkHistory := models.NewWorkHistoryData{
			JobTitle:   "Systems Analyst",
			Employer:   "Google",
			Location:   "San Francisco",
			StartMonth: "January",
			StartYear:  2016,
			Ended:      false,
			Summary:    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		workHistory, err := models.WorkHistoryModel.CreateWorkHistory(testUserID.Hex(), &newWorkHistory)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/work-history/%s", workHistory.ID.Hex()), nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			So(responseData.Message, ShouldEqual, "Work history deleted")
			// Check to make sure work history doesnt exist in database
			_, err := models.WorkHistoryModel.FindWorkHistoryByData(bson.M{"_id": bson.ObjectIdHex(workHistory.ID.Hex())})
			So(err, ShouldNotEqual, nil)
		})

	})
}
