package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestAwardItemCreateAndUpdateDataValidations(t *testing.T) {
	Convey("Test data validation for create and update award routes", t, func() {
		type routeTest struct {
			routeName string
			method    string
		}

		workHistoryRoutes := []routeTest{
			{routeName: "/award", method: "POST"},
			{routeName: "/award/%s", method: "PUT"},
		}

		for _, route := range workHistoryRoutes {
			Convey(fmt.Sprintf("%s - Data validation tests", route.routeName), func() {

				// Create test user
				// Create a working jwt token
				token, testUserID := generateJWTTokenForTestUser()

				// Create new item
				newAwardItemData := models.NewAwardData{
					Title:       "Best Award",
					Institution: "University of Essex",
					Website:     "http://google.com",
					Month:       "January",
					Year:        2015,
					Summary:     "Lorem ipsum dolor sit amet",
				}
				awardItem, err := models.AwardModel.CreateAwardItem(testUserID.Hex(), &newAwardItemData)
				if err != nil {
					panic(err)
				}

				var actualRoute string
				if route.method == "POST" {
					actualRoute = route.routeName
				}
				if route.method == "PUT" {
					actualRoute = fmt.Sprintf(route.routeName, awardItem.ID.Hex())
				}

				Convey("Empty data", func() {
					req, _ := http.NewRequest(route.method, actualRoute, nil)
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseMessageJSON(response.Body)
					Convey("Bad request status code with message", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Please send some data")
					})
				})

				Convey("Empty JSON data", func() {
					payload := []byte(`{}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["institution"], ShouldEqual, "Institution name is required")
						So(responseData.Errors["title"], ShouldEqual, "Title is required")
						So(responseData.Errors["month"], ShouldEqual, "Month is required")
						So(responseData.Errors["year"], ShouldEqual, "Year is required")
						So(responseData.Errors["summary"], ShouldEqual, "Award summary is required")
					})
				})

				Convey("Long institution name error", func() {
					payload := []byte(`{
						"institution": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare eget odio metus Phasellus ornare eget odio metus."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["institution"], ShouldEqual, "Institution name can be maximum of 100 characters long")
					})
				})

				Convey("Long title error", func() {
					payload := []byte(`{
						"title": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare eget odio metus Phasellus ornare eget odio metus."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["title"], ShouldEqual, "Title can be maximum of 100 characters long")
					})
				})

				Convey("Invalid webiste error", func() {
					payload := []byte(`{
						"website": "Lorem ipsum dol"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["website"], ShouldEqual, "Website must be valid url")
					})
				})

				Convey("Long month error", func() {
					payload := []byte(`{
						"month": "Lorem ipsum dolor sit amet consectetur adipiscing elit hasellus ornare eget odio metus Phasellus ornare eget odio metus"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["month"], ShouldEqual, "Month can be maximum of 10 characters long")
					})
				})

				Convey("Invalid month error", func() {
					payload := []byte(`{
						"month": "sd032£@$*"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["month"], ShouldEqual, "Month can only contain letters")
					})
				})

				Convey("Invalid year error", func() {
					payload := []byte(`{
						"year": "lorem"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid data")
					})
				})

				Convey("Long summary error", func() {
					payload := []byte(`{
						"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet elit gravida, blandit dui vel, commodo sapien. Quisque non vulputate augue, sed tincidunt turpis. Phasellus suscipit tortor leo, a rhoncus tortor ullamcorper eget. Praesent pharetra quam a pharetra porttitor. Ut sit amet libero scelerisque ex egestas maximus quis vel sem. Maecenas tincidunt, nunc vitae aliquam laoreet, nunc nunc lobortis eros, ut fringilla orci massa at est. In consectetur, justo a mollis auctor, turpis elit euismod felis, eget aliquam nisi nibh eget turpis. In eu placerat dolor. Vivamus a tincidunt sed."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["summary"], ShouldEqual, "Award summary can be maximum of 500 characters long")
					})
				})

			})
		}
	})
}
