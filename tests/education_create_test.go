package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	"gopkg.in/mgo.v2/bson"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateEducationItem(t *testing.T) {
	// Create a working jwt token
	token, testUserID := generateJWTTokenForTestUser()

	Convey("Test add education item", t, func() {

		Convey("Add valid education item", func() {
			payload := []byte(`{
				"institution": "University of Essex",
				"location": "Colchester, Essex",
				"startMonth": "September",
				"startYear": 2015,
				"ended": false,
				"summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat ac, molestie pulvinar felis. Integer hendrerit et erat vel tristique. Morbi id elit eu ligula varius volutpat. Etiam mollis felis sit amet nunc consequat metus."
			}`)
			req, _ := http.NewRequest("POST", "/education", bytes.NewBuffer(payload))
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
			response := executeRequest(req)
			responseData := decodeResponseMessageJSON(response.Body)
			Convey("Created request status code with message", func() {
				So(response.Code, ShouldEqual, http.StatusCreated)
				So(responseData.StatusCode, ShouldEqual, http.StatusCreated)
				So(responseData, ShouldNotBeEmpty)
				So(responseData.Message, ShouldEqual, "Education item created")
			})
			foundEducationItem, err := models.EducationModel.FindEducationItemByData(bson.M{
				"userID": bson.ObjectIdHex(testUserID.Hex()),
			})
			So(err, ShouldEqual, nil)
			So(foundEducationItem.Institution, ShouldEqual, "University of Essex")
			So(foundEducationItem.Location, ShouldEqual, "Colchester, Essex")
			So(foundEducationItem.StartMonth, ShouldEqual, "September")
			So(foundEducationItem.StartYear, ShouldEqual, 2015)
			So(foundEducationItem.Ended, ShouldEqual, false)
			So(foundEducationItem.Summary, ShouldEqual, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis odio elit, maximus vel erat ac, molestie pulvinar felis. Integer hendrerit et erat vel tristique. Morbi id elit eu ligula varius volutpat. Etiam mollis felis sit amet nunc consequat metus.")
		})

	})

}
