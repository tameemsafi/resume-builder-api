package tests

import (
	"net/http"
	"resume-api/utils"
	"testing"

	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRestrictedRoutes(t *testing.T) {
	Convey("Test generate new tokens handler", t, func() {

		type routeTest struct {
			routeName string
			method    string
		}

		restrictedRoutes := []routeTest{
			// Profile / Authentication
			{routeName: "generate-token", method: "GET"},
			{routeName: "avatar", method: "POST"},
			{routeName: "profile", method: "PUT"},
			// Work history
			{routeName: "work-histories", method: "GET"},
			{routeName: "work-history", method: "POST"},
			{routeName: "work-history/{id}", method: "PUT"},
			{routeName: "work-history/{id}", method: "DELETE"},
			// Education
			{routeName: "educations", method: "GET"},
			{routeName: "education", method: "POST"},
			{routeName: "education/{id}", method: "PUT"},
			{routeName: "education/{id}", method: "DELETE"},
			// Awards
			{routeName: "awards", method: "GET"},
			{routeName: "award", method: "POST"},
			{routeName: "award/{id}", method: "PUT"},
			{routeName: "award/{id}", method: "DELETE"},
			// Skills
			{routeName: "skills", method: "GET"},
			{routeName: "skill", method: "POST"},
			{routeName: "skill/{id}", method: "PUT"},
			{routeName: "skill/{id}", method: "DELETE"},
			// Languages
			{routeName: "languages", method: "GET"},
			{routeName: "language", method: "POST"},
			{routeName: "language/{id}", method: "PUT"},
			{routeName: "language/{id}", method: "DELETE"},
			// Hobbies
			{routeName: "hobbies", method: "GET"},
			{routeName: "hobbie", method: "POST"},
			{routeName: "hobbie/{id}", method: "PUT"},
			{routeName: "hobbie/{id}", method: "DELETE"},
			// References
			{routeName: "references", method: "GET"},
			{routeName: "reference", method: "POST"},
			{routeName: "reference/{id}", method: "PUT"},
			{routeName: "reference/{id}", method: "DELETE"},
		}

		for _, route := range restrictedRoutes {
			Convey(fmt.Sprintf("Unauthorized access for /%s with %s method", route.routeName, route.method), func() {
				req, _ := http.NewRequest(route.method, fmt.Sprintf("/%s", route.routeName), nil)
				response := executeRequest(req)
				Convey("Unauthorized status code with message", func() {
					So(response.Code, ShouldEqual, http.StatusUnauthorized)
					So(response.Body.String(), ShouldEqual, "Required authorization token not found\n")
				})
			})
		}

	})
}

func TestGenerateToken(t *testing.T) {
	Convey("Generate a new token", t, func() {
		// Create a working jwt token
		token, _ := generateJWTTokenForTestUser()
		// Send request for a new token
		req, _ := http.NewRequest("GET", "/generate-token", nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		responseData := decodeResponseMessageJSON(response.Body)
		Convey("OK status code with valid jwt token message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			newTokenValidationError := utils.JWT.VerifyEncodedJWT(responseData.Message.(string))
			So(newTokenValidationError, ShouldEqual, nil)
		})
	})
}
