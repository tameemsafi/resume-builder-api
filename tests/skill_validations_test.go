package tests

import (
	"bytes"
	"fmt"
	"net/http"
	"resume-api/models"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSkillItemCreateAndUpdateDataValidations(t *testing.T) {
	Convey("Test data validation for create and update skill routes", t, func() {
		type routeTest struct {
			routeName string
			method    string
		}

		workHistoryRoutes := []routeTest{
			{routeName: "/skill", method: "POST"},
			{routeName: "/skill/%s", method: "PUT"},
		}

		for _, route := range workHistoryRoutes {
			Convey(fmt.Sprintf("%s - Data validation tests", route.routeName), func() {

				// Create test user
				// Create a working jwt token
				token, testUserID := generateJWTTokenForTestUser()

				// Create new item
				newItem := models.NewSkillData{
					Title: "PHP",
					Level: "Expert",
				}
				item, err := models.SkillModel.CreateSkillItem(testUserID.Hex(), &newItem)
				if err != nil {
					panic(err)
				}

				var actualRoute string
				if route.method == "POST" {
					actualRoute = route.routeName
				}
				if route.method == "PUT" {
					actualRoute = fmt.Sprintf(route.routeName, item.ID.Hex())
				}

				Convey("Empty data", func() {
					req, _ := http.NewRequest(route.method, actualRoute, nil)
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseMessageJSON(response.Body)
					Convey("Bad request status code with message", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Please send some data")
					})
				})

				Convey("Empty JSON data", func() {
					payload := []byte(`{}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["title"], ShouldEqual, "Skill title is required")
						So(responseData.Errors["level"], ShouldEqual, "Skill level is required")
					})
				})

				Convey("Long title error", func() {
					payload := []byte(`{
						"title": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare eget odio metus Phasellus ornare eget odio metus."
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["title"], ShouldEqual, "Skill title can be maximum of 100 characters long")
					})
				})

				Convey("Long level error", func() {
					payload := []byte(`{
						"level": "Lorem ipsum dolLorem ipsum dol"
					}`)
					req, _ := http.NewRequest(route.method, actualRoute, bytes.NewBuffer(payload))
					req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
					response := executeRequest(req)
					responseData := decodeResponseErrorsJSON(response.Body)
					Convey("Bad request status code with errors", func() {
						So(response.Code, ShouldEqual, http.StatusBadRequest)
						So(responseData.StatusCode, ShouldEqual, http.StatusBadRequest)
						So(responseData, ShouldNotBeEmpty)
						So(responseData.Message, ShouldEqual, "Invalid request")
						So(responseData.Errors["level"], ShouldEqual, "Skill level can be maximum of 15 characters long")
					})
				})

			})
		}
	})
}
