package tests

import (
	"fmt"
	"net/http"
	"resume-api/controllers"
	"resume-api/models"
	"testing"

	"encoding/json"

	"reflect"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReadWorkHistories(t *testing.T) {
	Convey("Read all work histories", t, func() {
		// Create a working jwt token
		token, testUserID := generateJWTTokenForTestUser()
		// Create work histories for user
		newWorkHistoryOne := models.NewWorkHistoryData{
			JobTitle:   "Systems Analyst",
			Employer:   "Google",
			Location:   "San Francisco",
			StartMonth: "January",
			StartYear:  2016,
			Ended:      false,
			Summary:    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		newWorkHistoryTwo := models.NewWorkHistoryData{
			JobTitle:   "Web Developer",
			Employer:   "LinkedIn",
			Location:   "San Francisco",
			StartMonth: "January",
			StartYear:  2015,
			EndMonth:   "December",
			EndYear:    2015,
			Ended:      true,
			Summary:    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		newWorkHistoryThree := models.NewWorkHistoryData{
			JobTitle:   "Technician",
			Employer:   "Apple",
			Location:   "San Francisco",
			StartMonth: "January",
			StartYear:  2014,
			EndMonth:   "December",
			EndYear:    2014,
			Ended:      true,
			Summary:    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit ligula non malesuada ullamcorper. Ut dictum placerat eros, nec finibus orci sollicitudin id. Curabitur nec nibh sed sapien pharetra luctus id quis libero.",
		}
		workHistoryOne, err := models.WorkHistoryModel.CreateWorkHistory(testUserID.Hex(), &newWorkHistoryOne)
		if err != nil {
			panic(err)
		}
		workHistoryTwo, err := models.WorkHistoryModel.CreateWorkHistory(testUserID.Hex(), &newWorkHistoryTwo)
		if err != nil {
			panic(err)
		}
		workHistoryThree, err := models.WorkHistoryModel.CreateWorkHistory(testUserID.Hex(), &newWorkHistoryThree)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("GET", "/work-histories", nil)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		response := executeRequest(req)
		var responseData controllers.WorkHistoriesResponse
		json.NewDecoder(response.Body).Decode(&responseData)
		Convey("OK request status code with message", func() {
			So(response.Code, ShouldEqual, http.StatusOK)
			So(responseData.StatusCode, ShouldEqual, http.StatusOK)
			So(responseData, ShouldNotBeEmpty)
			for k, responseWorkHistory := range responseData.WorkHistories {
				var validReflection reflect.Value
				if k == 0 {
					validReflection = reflect.ValueOf(&workHistoryOne).Elem()
				}
				if k == 1 {
					validReflection = reflect.ValueOf(&workHistoryTwo).Elem()
				}
				if k == 2 {
					validReflection = reflect.ValueOf(&workHistoryThree).Elem()
				}
				reflection := reflect.ValueOf(&responseWorkHistory).Elem()
				for i := 0; i < reflection.NumField(); i++ {
					So(reflection.Field(i).String(), ShouldEqual, validReflection.Field(i).String())
				}
			}
		})
	})
}
